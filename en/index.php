<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "5th Mexican Congress Of Tunnel Engineering and Underground Works | november - december 2023 | Mexico City";
$description = "5th Mexican Congress Of Tunnel Engineering and Underground Works. November 29<sup>th</sup> and 30<sup>th</sup>, December 1 <sup>st</sup>, 2023. Mexico City";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2023 -->
    <? include_once("../include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2023 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/"
                                    target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/"
                                    target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/en/index.php">
                                    <img class="logo" src="../img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2023 -->
                                <? include_once("../include/navbar_en.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2023 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contact
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - CONTENIDOS INDEX 5CMITOS WEB 2023 -->
        <div id="content" class="no-bottom no-top">

            <!-- INICIO - PRESENTACION 5CMITOS WEB 2023 -->
            <section id="section-hero" class="vertical-center jarallax text-light" aria-label="section">
                <img src="../img/index/amitos_congreso_2023.webp" class="jarallax-img" alt="">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="spacer-single"></div>
                        <div class="col-lg-5 mb-sm-30">
                            <h2 class="style-5">
                                <br> November 29<sup>th</sup> and 30<sup>th</sup>
                                <br> December 1<sup>st</sup>
                                <br> 2023
                            </h2>
                            <h3>
                                Congress Venue
                            </h3>
                            <p class="text-large text-white">
                                College of Civil Engineers of Mexico, Camino Santa Teresa 187, Parques del Pedregal,
                                Tlalpan, C.P. 14010, Ciudad de México, México.
                            </p>
                            <!--<a href="#section-features" class="btn-custom text-light">
									Our Services
								</a>-->
                            <div class="spacer-single"></div>
                        </div>

                        <div class="col-lg-6 offset-lg-1 text-middle">
                            <img src="../img/logo/logo_5cmitos_g.webp" alt="" class="img-responsive" />
                        </div>
                    </div>
                    <div class="spacer-single"></div>
                </div>
            </section>
            <!-- FIN - PRESENTACION 5CMITOS WEB 2023 -->

            <!-- INICIO - COLUMNAS IMG INFO GENERAL 5CMITOS WEB 2023 -->
            <section id="section-services" class="no-top no-bottom  mt-50">
                <div class="container">
                    <div class="row g-0">

                        <div class="col-lg-3 sm-mb-30 wow fadeInUp" data-wow-delay="0s">

                            <img src="../img/index/amitos_congreso_img_ch_01.webp" class="img-responsive" alt="">

                            <div class="text padding30" data-bgcolor="#f2f2f2">
                                <h3>
                                    November 29<sup>th</sup>
                                </h3>
                                <p class="mb10">
                                    <i>Preconference courses</i><br>
                                    <br> - Conventional Tunnels and Their Additional Works, Planning and Construction.
                                    <br> - Mechanized Tunnels, Microtunnels, and its Shafts Access, Planning and
                                    Construction.
                                    <br> - Numerical Analysis Of Underground Works.
                                    <br> - Instrumentation of Underground Works.
                                </p>
                            </div>

                        </div>

                        <div class="col-lg-3 sm-mb-30 wow fadeInUp" data-wow-delay=".3s">

                            <div class="text padding30" data-bgcolor="#f6f6f6">
                                <h3>
                                    November 30<sup>th</sup> and December 1<sup>st</sup>
                                </h3>
                                <p class="mb10">
                                    <i>Technical Sessions</i>
                                    <br> - Technology and innovation in the engineering of underground works.
                                    <br> - Mining and underground works.
                                    <br> - Numerical methods in underground works.
                                    <br> - Geothermal energy and singular projects in underground works.
                                    <br> - Microtunnels and trenchless excavation technology.
                                </p>
                            </div>

                            <img src="../img/index/amitos_congreso_img_ch_02.webp" class="img-responsive" alt="">

                        </div>

                        <div class="col-lg-3 sm-mb-30 wow fadeInUp" data-wow-delay=".6s">

                            <img src="../img/index/amitos_congreso_img_ch_03.webp" class="img-responsive" alt="">

                            <div class="text padding30" data-bgcolor="#f2f2f2">
                                <h3>
                                    Abstract submission deadline for technical sessions:
                                </h3>
                                <p class="mb10">
                                    June 9, 2023. <br>
                                    <br> The abstracts must be sent to the mail:
                                    <br><br> <strong>amitos@amitos.org</strong>
                                    <br><br> The format will be hybrid (classroom and distance learning).
                                </p>
                            </div>

                        </div>

                        <div class="col-lg-3 sm-mb-30 wow fadeInUp" data-wow-delay=".9s">

                            <div class="text padding30" data-bgcolor="#f6f6f6">
                                <p class="mb10">
                                    This XVIII Board of Directors extends a cordial invitation to join this important
                                    event.
                                    <br><br> Save the dates!
                                    <br><br> <strong>Let's project the image of our future and make a tunnel to get
                                        there.</strong>
                                </p>
                            </div>

                            <img src="../img/index/amitos_congreso_img_ch_04.webp" class="img-responsive" alt="">

                        </div>

                    </div>
                </div>
            </section>
            <!-- FIN - COLUMNAS IMG INFO GENERAL 5CMITOS WEB 2023 -->

            <!-- INICIO - CALENDARIO DEL EVENTO 5CMITOS WEB 2023 -->
            <section class="de_light bg-white">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12 text-center">
                            <h2>
                                Event Schedule
                            </h2>
                        </div>

                        <div class="col-md-6 offset-md-3">
                            <div class="timeline exp">

                                <div class="tl-block wow fadeInUp" data-wow-delay="0">
                                    <div class="tl-time">
                                        <h4>
                                            June 9<sup>th</sup>
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Abstract submission deadline for technical sessions.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".3s">
                                    <div class="tl-time">
                                        <h4>
                                            June 16<sup>th</sup>
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Deadline for abstract acceptance.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".6s">
                                    <div class="tl-time">
                                        <h4>
                                            August 15<sup>th</sup>
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Deadline for submitting articles.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".9s">
                                    <div class="tl-time">
                                        <h4>
                                            August 22<sup>th</sup>
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Deadline for notification of acceptance of articles.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".12s">
                                    <div class="tl-time">
                                        <h4>
                                            October 2<sup>nd</sup>
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Deadline for oral presentation notification.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".15s">
                                    <div class="tl-time">
                                        <h4>
                                            November 1<sup>st</sup>
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Deadline for registration of persons who will present papers at the
                                                Congress.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <hr>

                    </div>
                </div>
            </section>
            <!-- FIN - CALENDARIO DEL EVENTO 5CMITOS WEB 2023 -->

            <!-- INICIO - CALENDARIO INTERNO CURSOS SESIONES 5CMITOS WEB 2023
            <section class="de_light bg-white">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12 text-center">
                            <h2>
                                Internal Calendar, Courses and Technical Sessions.
                            </h2>
                        </div>

                        <div class="col-md-6 offset-md-3">
                            <div class="timeline exp">

                                <div class="tl-block wow fadeInUp" data-wow-delay="0">
                                    <div class="tl-time">
                                        <h4>
                                            June 23<sup>th</sup>
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Deadline to submit proposals for keynote speakers.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".3s">
                                    <div class="tl-time">
                                        <h4>
                                            June 23<sup>th</sup>
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Deadline to submit course syllabus and names of keynote
                                                speakers.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <hr>

                    </div>
                </div>
            </section>
            FIN - CALENDARIO INTERNO CURSOS SESIONES 5CMITOS WEB 2023 -->

            <!-- INICIO - INFO INTRO 5CMITOS WEB 2023 -->
            <section id="section-text">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-4 offset-md-1 sm-mb-30 text-center wow fadeInRight">
                            <div class="de-images">
                                <img class="di-small wow fadeIn" src="../img/index/amitos_congreso_img_ch_05.webp"
                                    alt="" />
                                <img class="di-small-2" src="../img/index/amitos_congreso_img_ch_07.webp" alt="" />
                                <img class="img-fluid wow fadeInRight" data-wow-delay=".25s"
                                    src="../img/index/amitos_congreso_img_ch_06.webp" alt="" />
                            </div>
                        </div>

                        <div class="col-lg-5 offset-md-1 wow fadeInLeft" data-wow-delay="0s">
                            <h3 class="mb20">
                                This 2023, the Mexican Association of Engineering
                                of Tunnels and Underground Works, also known as
                                AMITOS, turns 40 years old.
                            </h3>
                            <p>
                                It was founded on June 13, 1983. <strong>The Fifth AMITOS Mexican Congress will be the
                                    opportunity to commemorate these 40 years of development of the tunnel engineering
                                    and Mexican underground works</strong>, as well as honor those who have left their
                                mark in this history.
                                <br><br>
                                Based in CDMX, <strong>it will consist of four courses pre-congress, five technical
                                    sessions, as well as 10 master conferences with national speakers and
                                    international</strong> . In addition, we will have an area commercial for exhibitors
                                The event will conclude with the participation of a special guest, who will talk about
                                foreign cultural issues and not so foreign to engineering.
                                <br><br>
                                We hope to have a significant participation of the new
                                generations of engineers.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- FIN - INFO INTRO 5CMITOS WEB 2023 -->

            <!-- INICIO - 40 AÑOS DE AMITOS INFO 5CMITOS WEB 2023 -->
            <section id="section-action" class="jarallax text-light" aria-label="cta">
                <img src="../img/index/amitos_congreso_2023_2.webp" class="jarallax-img" alt="">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-4 wow fadeInUp" data-wow-delay=".2s">
                            <div class="de_count ultra-big text-center">
                                <h3 class="timer" data-to="40" data-speed="3000">
                                    1
                                </h3>
                                <span class="text-white">
                                    Years Developing the Underground Space
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-4 wow fadeInUp">
                            <h2>
                                Mexican Association of Tunnel Engineering and Underground Works.
                            </h2>
                        </div>
                        <div class="col-lg-4 wow fadeInUp">
                            <p class="text-large">
                                Don't forget! to register for the
                                <br> 5<sup>th</sup> Congress AMITOS 2023.
                            </p>
                            <a href="https://5congresoamitos.com.mx/registro/" target="blank"
                                class="btn-custom text-light wow fadeInUp">
                                ¡Sign up!
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- FIN - 40 AÑOS DE AMITOS INFO 5CMITOS WEB 2023 -->

            <!-- INICIO - ICONOS NAVEGACIÓN INFO 5CMITOS WEB 2023 -->
            <section id="section-text-2">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-4 wow fadeIn" data-wow-delay="0s">
                            <div class="box-number square sm-mb-30">
                                <i class="icon_documents bg-color text-white"></i>
                                <div class="text">
                                    <h3>
                                        Technical schedule
                                    </h3>
                                    <p>
                                        Learn about the series of activities to be held at the AMITOS 2023 Congress.
                                    </p>
                                    <a href="programa_tecnico_congresoamitos_2023.php"
                                        class="btn-custom text-light wow fadeInUp">
                                        Know more
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 wow fadeIn" data-wow-delay=".5s">
                            <div class="box-number square sm-mb-30">
                                <i class="icon_mic bg-color text-white"></i>
                                <div class="text">
                                    <h3>
                                        Expo
                                    </h3>
                                    <p>
                                        We invite you to visit the stands of the congress participants.
                                    </p>
                                    <a href="expo_congresoamitos_2023.php" class="btn-custom text-light wow fadeInUp">
                                        Know more
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 wow fadeIn" data-wow-delay="1s">
                            <div class="box-number square sm-mb-30">
                                <i class="icon_creditcard bg-color text-white"></i>
                                <div class="text">
                                    <h3>
                                        Pricing
                                    </h3>
                                    <p>
                                        You already know the costs for the 5th AMITOS Congress 2023!
                                    </p>
                                    <a href="costos_inscripcion_congresoamitos_2023.php"
                                        class="btn-custom text-light wow fadeInUp">
                                        Know more
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- FIN - ICONOS NAVEGACIÓN INFO 5CMITOS WEB 2023 -->

            <!-- INICIO - PATROCINIOS 5CMITOS WEB 2023 -->
            <?php include_once("../include/patrocinadores_congresoamitos_2023_en.php"); ?>
            <!-- FIN - PATROCINIOS 5CMITOS WEB 2023 -->

        </div>
        <!-- FIN - CONTENIDOS INDEX 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("../include/footer_en.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("../include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

    <!-- INICIO - JS AUTOPLAY PATROCINADORES 5CMITOS 2023 -->
    <script>
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            items: 6,
            loop: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true
        });
    </script>
    <!-- FIN - JS AUTOPLAY PATROCINADORES 5CMITOS 2023 -->

</body>

</html>