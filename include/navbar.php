<nav class="md-flex">
    <ul id="mainmenu">

        <li>
            <a href="#">
                Comités
            </a>
            <ul>
                <li>
                    <a href="<?= $servidor ?>/comites_congresoamitos_2023.php">
                        Comité Organizador
                    </a>
                </li>
                <li>
                    <a href="<?= $servidor ?>/presidentes_sesion_congresoamitos_2023.php">
                        Presidentes de sesión
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="#">
                Programas
            </a>
            <ul>
                <li>
                    <a href="<?= $servidor ?>/cursos_precongreso_congresoamitos_2023.php">
                        Cursos Precongreso
                    </a>
                </li>
                <li>
                    <a href="<?= $servidor ?>/conferencias_precongreso_congresoamitos_2023.php">
                        Conferencias Precongreso
                    </a>
                </li>
                <li>
                    <a href="<?= $servidor ?>/programa_tecnico_congresoamitos_2023.php">
                        Programa Técnico
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="<?= $servidor ?>/expo_congresoamitos_2023.php">
                Expo
            </a>
            <!--<ul>
                <li>
                    <a href="#">
                        Residential Design
                    </a>
                </li>
                <li>
                    <a href="#">
                        Hospitaly Design
                    </a>
                </li>
                <li>
                    <a href="#">
                        Office Design
                    </a>
                </li>
                <li>
                    <a href="#">
                        Commercial Design
                    </a>
                </li>
                <li>
                    <a href="#">
                        All Services
                    </a>
                </li>
                <li>
                    <a href="#">
                        New All Services
                    </a>
                </li>
            </ul>-->
        </li>

        <li>
            <a href="<?= $servidor ?>/costos_inscripcion_congresoamitos_2023.php">
                Costos
            </a>
        </li>

        <li>
            <a href="<?= $servidor ?>/contacto_congresoamitos_2023.php">
                Contacto
            </a>
        </li>

        <li>
            <a href="https://5congresoamitos.com.mx/registro/" target="_blank">
                Registro
            </a>
        </li>

        <li>
            <a href="boletin/boletin_5congresoamitos_noviembre_2023_v3.pdf" download>
                Boletín
            </a>
        </li>

        <li>
            <a href="<?= $servidor ?>/en/index.php">
                ENG
            </a>
        </li>

    </ul>
</nav>
<!-- mainmenu close -->


