<!-- INICIO - JAVASCRIPT FILES -->
<script src="<?= $js ?>/plugins.js"></script>
<script src="<?= $js ?>/designesia.js"></script>
<script src="<?= $js ?>/jquery.plugin.js"></script>
<script src="<?= $js ?>/jquery.countdown.js"></script>
<script src="<?= $js ?>/countdown-custom.js"></script>
<script src="<?= $js ?>/jquery.event.move.js"></script>
<script src="<?= $js ?>/jquery.twentytwenty.js"></script>

<!-- INICIO - SLIDER REVOLUTION SCRIPTS  -->
<script src="<?= $rsplugin ?>/js/jquery.themepunch.plugins.min.js"></script>
<script src="<?= $rsplugin ?>/js/jquery.themepunch.revolution.min.js"></script>

<script>
    $(window).on("load", function() {
        $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({
            default_offset_pct: 0.5
        });
        $(".twentytwenty-container[data-orientation='vertical']").twentytwenty({
            default_offset_pct: 0.5,
            orientation: 'vertical'
        });
    });
</script>