<!-- INICIO - PATROCINIOS 5CMITOS WEB 2023 -->
        <section class="text-light" data-stellar-background-ratio=".1">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 col-sm-6 col-xs-6 wow fadeInUp">
                        <div id="logo-carousel" class="owl-carousel owl-theme">

                            <a href="https://www.amitos.org/" target="_blank">
                                <img src="img/patrocinadores/logo_amitos_5congresoamitos_2023.webp" class="img-responsive" alt="amitos, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.ita-aites.org/" target="_blank">
                                <img src="img/patrocinadores/logo_ita_aites_5congresoamitos_2023.webp" class="img-responsive" alt="ITA, AITES, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="http://www.aldesa.com.mx/" target="_blank">
                                <img src="img/patrocinadores/logo_aldesa.webp" class="img-responsive" alt="aldesa, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.algaher.com/en/" target="_blank">
                                <img src="img/patrocinadores/logo_algaher.webp" class="img-responsive" alt="algaher, patrocinadores, congreso, amitos, 2023">
                            </a>

                             <a href="http://www.alianzafiidem.org/index.html" target="_blank">
                                <img src="img/patrocinadores/logo_alianza_fidem.webp" class="img-responsive" alt="alianza, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://barchip.com/" target="_blank">
                                <img src="img/patrocinadores/logo_barchipinc_5congresoamitos_2023.webp" class="img-responsive" alt="barchip inc, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="http://www.bessac.com.mx/" target="_blank">
                                <img src="img/patrocinadores/logo_bessac_5congresoamitos_2023.webp" class="img-responsive" alt="bessac, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://promotoraberma.com/" target="_blank">
                                <img src="img/patrocinadores/logo_berma_5congresoamitos_2023.webp" class="img-responsive" alt="berma, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.carpitech.com/en/home/" target="_blank">
                                <img src="img/patrocinadores/logo_carpi.webp" class="img-responsive" alt="carpi, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.deacero.com/" target="_blank">
                                <img src="img/patrocinadores/logo_deacero.webp" class="img-responsive" alt="deacero, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="http://dicyg.fi-c.unam.mx:8080/Site/" target="_blank">
                                <img src="img/patrocinadores/dicyg.webp" class="img-responsive" alt="DICyG, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.dsiunderground.com/es/latam" target="_blank">
                                <img src="img/patrocinadores/logo_dsiunderground_5congresoamitos_2023.webp" class="img-responsive" alt="DSI Underground, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="http://www.freyssinet.com.mx/" target="_blank">
                                <img src="img/patrocinadores/logo_freyssinet.webp" class="img-responsive" alt="freyssinet, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="http://www.fundacion-ica.org.mx/" target="_blank">
                                <img src="img/patrocinadores/logo_fundacion_ica.webp" class="img-responsive" alt="fundación ica, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.geoygeo.com/" target="_blank">
                                <img src="img/patrocinadores/logo_gg.webp" class="img-responsive" alt="Geomembranas & Geosint, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.gdi.com.mx/" target="_blank">
                                <img src="img/patrocinadores/logo_grupo_desarrollo_infraestructura.webp" class="img-responsive" alt="grupo desarrollo infraestructura, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="http://www.gsstitan.com/" target="_blank">
                                <img src="img/patrocinadores/logo_gss_titan.webp" class="img-responsive" alt="Gss, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.triada.com.mx/" target="_blank">
                                <img src="img/patrocinadores/logo_grupo_triada_5congresoamitos_2023.webp" class="img-responsive" alt="Grupo Triada, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.herrenknecht.com/en/" target="_blank">
                                <img src="img/patrocinadores/logo_herrenknecht_5congresoamitos_2023.webp" class="img-responsive" alt="herrenknecht, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="http://ica.com.mx/" target="_blank">
                                <img src="img/patrocinadores/logo_ica.webp" class="img-responsive" alt="ica, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="http://www.iingen.unam.mx/es-mx/Paginas/Splash/Default.aspx" target="_blank">
                                <img src="img/patrocinadores/logo_iiunam_5congresoamitos_2023.webp" class="img-responsive" alt="iiunam, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.istt.com/" target="_blank">
                                <img src="img/patrocinadores/istt.webp" class="img-responsive" alt="ISTT, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.knightpiesold.com/en/" target="_blank">
                                <img src="img/patrocinadores/logo_knightpiesold_5congresoamitos_2023.webp" class="img-responsive" alt="Knight Piesold, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://minearc.com/" target="_blank">
                                <img src="img/patrocinadores/logo_minearc.webp" class="img-responsive" alt="minearc, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://utt.mapei.com/en/home-page" target="_blank">
                                <img src="img/patrocinadores/logo_mapei.webp" class="img-responsive" alt="mapei, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://mbcc.sika.com/es-mx" target="_blank">
                                <img src="img/patrocinadores/logo_master_builders_solutions_5congresoamitos_2023.webp" class="img-responsive" alt="master builders solutions, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.moldequipo.com/" target="_blank">
                                <img src="img/patrocinadores/logo_moldeequipo_internacional_5congresoamitos_2023.webp" class="img-responsive" alt="molde equipo, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.lombardi.ch/es-es" target="_blank">
                                <img src="img/patrocinadores/logo_lombardi_5congresoamitos_2023.webp" class="img-responsive" alt="lombardi, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="http://www.lytsa.com.mx/" target="_blank">
                                <img src="img/patrocinadores/logo_lytsa_lumbreras_tuneles_5congresoamitos_2023.webp" class="img-responsive" alt="lytsa, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://o-tek.com/" target="_blank">
                                <img src="img/patrocinadores/logo_otek_5congresoamitos_2023.webp" class="img-responsive" alt="otek, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.palmierigroup.com/" target="_blank">
                                <img src="img/patrocinadores/palmieri.webp" class="img-responsive" alt="Palmieri, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.robbinstbm.com/es/" target="_blank">
                                <img src="img/patrocinadores/logo_robbins.webp" class="img-responsive" alt="robbins, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.rocscience.com/" target="_blank">
                                <img src="img/patrocinadores/logo_roscience_5congresoamitos_2023.webp" class="img-responsive" alt="rocscience, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.latinoamerica.sisgeo.com/es/" target="_blank">
                                <img src="img/patrocinadores/sisgeo.webp" class="img-responsive" alt="sisgeo, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.smig.org.mx/" target="_blank">
                                <img src="img/patrocinadores/logo_smig.webp" class="img-responsive" alt="smig, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://temocsa.com/" target="_blank">
                                <img src="img/patrocinadores/logo_temocsa.webp" class="img-responsive" alt="temocsa, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://terratec.co/" target="_blank">
                                <img src="img/patrocinadores/terratec.webp" class="img-responsive" alt="terratec, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.tidesa.com.mx/" target="_blank">
                                <img src="img/patrocinadores/logo_tidesa_5congresoamitos_2023.webp" class="img-responsive" alt="tidesa, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://elitemin.com/tm/" target="_blank">
                                <img src="img/patrocinadores/logo_tunnel_mining.webp" class="img-responsive" alt="tunnel & mining, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://www.vise.com.mx/" target="_blank">
                                <img src="img/patrocinadores/logo_vise.webp" class="img-responsive" alt="vise, patrocinadores, congreso, amitos, 2023">
                            </a>

                            <a href="https://zitron.com/" target="_blank">
                                <img src="img/patrocinadores/logo_zitron.webp" class="img-responsive" alt="zitrón, patrocinadores, congreso, amitos, 2023">
                            </a>


                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- FIN - PATROCINIOS 5CMITOS WEB 2023 -->