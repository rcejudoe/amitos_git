<!-- INICIO - FOOTER 5CMITOS 2023 -->
<footer class="light">
    <div class="container">
        <div class="row">
            <hr width="20px">
            <div class="col-lg-3">
                <img src="<?= $img ?>/logo/logo_40_amitos_sf.png" class="logo-small" alt="">
                <br> 5 Mexican Congress Of Tunnel Engineering and Underground Works.
            </div>

            <div class="col-lg-3">
                <div class="widget widget_recent_post">
                    <h3 class="text-blue-dark">
                        AMITOS Congress 2023
                    </h3>
                    <ul>
                        <li>
                            <a href="<?= $servidor ?>/costos_inscripcion_congresoamitos_2023.php">
                                Pricing
                            </a>
                        </li>
                        <li>
                            <a href="<?= $servidor ?>/expo_congresoamitos_2023.php">
                                Expo
                            </a>
                        </li>
                        <li>
                            <a href="<?= $servidor ?>/programa_tecnico_congresoamitos_2023.php">
                                Technical schedule
                            </a>
                        </li>
                        <li>
                            <a href="<?= $servidor ?>/cursos_precongreso_congresoamitos_2023.php">
                                Preconference courses
                            </a>
                        </li>
                        <li>
                            <a href="<?= $servidor ?>/comites_congresoamitos_2023.php">
                                Committees
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="widget widget_recent_post">
                    <h3 class="text-blue-dark">
                        Registration
                    </h3>
                    <ul>
                        <li>
                            <a href="https://5congresoamitos.com.mx/registro/" target="_blank">
                                <strong class="text-red">Sign up</strong>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3">
                <h3 class="text-blue-dark">
                    Contacto
                </h3>
                <div class="widget widget-address">
                    <address>
                        <!--<span>
                            Camino a Santa Teresa 187 
                           <br> Col. Parques del Pedregal
                        </span>-->
                        <span>
                            <strong>Phone:</strong>
                            <a href="tel:+(52) 4579 8381">
                                (55) 4579-8381
                            </a>
                        </span>
                        <span>
                            <strong>Email:</strong>
                            <a href="mailto:amitos@amitos.org">
                                AMITOS Contact
                            </a>
                        </span>
                        <span>
                            <strong>Site:</strong>
                            <a href="https://www.amitos.org/" target="blank">
                                AMITOS
                            </a>
                        </span>
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div class="subfooter">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    &copy; Copyright 2023.
                    <br> 5 Mexican Congress Of Tunnel Engineering and Underground Works.
                    <br> Powered by <a href="https://www.puntozip.com.mx"><span class="id-color">Punto Zip.</span></a>
                </div>

                <div class="col-md-6 text-right text-large">
                    <div class="social-icons">
                        <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="blank">
                            <i class="fa fa-facebook fa-lg"></i>
                        </a>
                        <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="_blank">
                            <i class="fa fa-linkedin fa-lg"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <a href="#" id="back-to-top" class="light"></a>

</footer>
<!-- FIN - FOOTER 5CMITOS 2023 -->