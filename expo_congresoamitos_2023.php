<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "Expo | 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas | noviembre - diciembre 2023 | CDMX";
$description = "Expo. 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas. 29 y 30 de noviembre, 01 de diciembre, 2023. CDMX";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2020 -->
    <? include_once("include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2020 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/index.php">
                                    <img class="logo" src="img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2020 -->
                                <? include_once("include/navbar.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2020 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contacto
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - SUBHEADER EXPO 5CMITOS WEB 2023 -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            Expo
                        </h1>
                        <ul class="crumb">
                            <li>
                                <a href="<?= $servidor ?>/index.php">
                                    Inicio
                                </a>
                            </li>
                            <li class="sep">
                                /
                            </li>
                            <li>
                                <a href="expo_congresoamitos_2023.php">
                                    Expo
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - SUBHEADER EXPO 5CMITOS WEB 2023 -->

        <!-- INICIO - PRECIO STAND 5CMITOS WEB 2023 -->
        <section id="call-to-action" class="bg-color call-to-action padding40" aria-label="cta">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12 col-md-7">
                        <h3 class="text-dark size-2 no-margin text-white">
                            Precio por stand: $45,000 MNX (más IVA).
                            <br> *Cada espacio comercial stand incluye 3 inscripciones al congreso en
                            cortesía.
                            <br> *Las dimensiones de cada stand son 2x3m
                        </h3>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - PRECIO STAND 5CMITOS WEB 2023 -->

        <!-- INICIO - IMG EXPO 5CMITOS WEB 2023 -->
        <div id="content">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <div class="blog-read">
                            <div class="post-content">

                                <div class="text-center">
                                    <img src="img/expo/expo_amitos_2023_v7.webp" class="img-responsive" alt="mapa expo, colegio de ingenieros civiles de méxico, 5 congreso amitos, cdmx, 2023" />
                                </div>

                                <br><br>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <center>
                <a href="archivos/Manual_de_expositor_5_Congreso_AMITOS.pdf" target="_blank"
                class="btn-custom text-white text-large">
                    Ver manual de expositor
                </a>
            </center>
        </div>
        <!-- FIN - IMG EXPO 5CMITOS WEB 2023 -->

        

        <!-- INICIO - PATROCINIOS 5CMITOS WEB 2023 -->
        <?php include_once("include/patrocinadores_congresoamitos_2023.php"); ?>
        <!-- FIN - PATROCINIOS 5CMITOS WEB 2023 -->

        <section id="call-to-action" class="bg-color call-to-action text-light padding40" aria-label="cta">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8 col-md-7">
                        <h3 class="size-2 no-margin">
                            Descarga aquí el Boletín del 5 Congreso AMITOS 2023
                        </h3>
                    </div>

                    <div class="col-lg-4 col-md-5 text-right">
                        <a href="boletin/boletin_5congresoamitos_octubre_2023_v2.pdf" download class="btn-line-white wow fadeInUp">
                            Descargar
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("include/footer.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

    <!-- INICIO - JS AUTOPLAY PATROCINADORES 5CMITOS 2023 -->
    <script>
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            items: 6,
            loop: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true
        });
    </script>
    <!-- FIN - JS AUTOPLAY PATROCINADORES 5CMITOS 2023 -->


</body>

</html>