<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "Costos de Inscripción | 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas | noviembre - diciembre 2023 | CDMX";
$description = "Costos de Inscripción. 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas. 29 y 30 de noviembre, 01 de diciembre, 2023. CDMX";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2020 -->
    <? include_once("include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2020 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/"
                                    target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/"
                                    target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/index.php">
                                    <img class="logo" src="img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2020 -->
                                <? include_once("include/navbar.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2020 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contacto
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - SUBHEADER COSTOS 5CMITOS WEB 2023 -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            Costos de Inscripción
                        </h1>
                        <ul class="crumb">
                            <li>
                                <a href="<?= $servidor ?>/index.php">
                                    Inicio
                                </a>
                            </li>
                            <li class="sep">
                                /
                            </li>
                            <li>
                                <a href="costos_inscripcion_congresoamitos_2023.php">
                                    Costos
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - SUBHEADER COSTOS 5CMITOS WEB 2023 -->

        <!-- INICIO - CONTENIDOS COSTOS 5CMITOS WEB 2023 -->
        <div id="content" class="no-bottom no-top">

            <!-- INICIO - TABLAS COSTOS 5CMITOS WEB 2023 -->
            <section id="section-pricing-coworking">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="item pricing">
                                <div class="container">
                                    <div class="row">

                                        <!-- INICIO - TABLA INSCRIPCIÓN TEMPRANA COSTOS 5CMITOS WEB 2023 -->
                                        <!-- <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6 wow fadeInUp"
                                            data-wow-delay="0s">
                                            <div class="pricing-s1 light mb30">
                                                <div class="top">
                                                    <h2>
                                                        <strong>Inscripción temprana</strong>
                                                    </h2>
                                                </div>
                                                <div class="bottom">
                                                    <ul>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Asistentes <strong>(socios)</strong>
                                                            <strong class="text-red">| $5,500 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Asistentes <strong>(no socios)</strong>
                                                            <strong class="text-red">| $8,000 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Estudiantes <strong>(presencial o zoom)</strong>
                                                            <strong class="text-red">| $750 MXN
                                                            </strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Asistente + curso <strong>(socios)</strong>
                                                            <strong class="text-red">| $8,500 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Asistente + curso <strong>(no socios)</strong>
                                                            <strong class="text-red">| $13,500 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Estudiante + curso <strong>(presencial o zoom)</strong>
                                                            <strong class="text-red">| $2,000 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Solo Curso <strong>(socios)</strong>
                                                            <strong class="text-red">| $5,500 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Solo Curso <strong>(no socios)</strong>
                                                            <strong class="text-red">| $8,000 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Asistente <strong>ZOOM *</strong>
                                                            <strong class="text-red">| $5,000 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Curso <strong>ZOOM *</strong>
                                                            <strong class="text-red">| $5,000 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Asistente + Curso <strong>ZOOM*</strong>
                                                            <strong class="text-red">| $8,000 MXN</strong>
                                                        </li>
                                                    </ul>
                                                    <a href="https://5congresoamitos.com.mx/registro/" target="_blank"
                                                        class="btn-custom text-white text-large">
                                                        ¡Regístrate aquí!
                                                    </a>
                                                </div>
                                            </div>
                                        </div> -->
                                        <!-- FIN - TABLA INSCRIPCIÓN TEMPRANA COSTOS 5CMITOS WEB 2023 -->

                                        <!-- INICIO - TABLA INSCRIPCIÓN NORMAL COSTOS 5CMITOS WEB 2023 -->
                                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6 wow fadeInUp"
                                            data-wow-delay=".3s">
                                            <div class="pricing-s1 light mb30">
                                                <div class="top">
                                                    <h2>
                                                        <strong>Inscripción normal</strong>
                                                    </h2>
                                                </div>
                                                <div class="bottom">
                                                    <ul>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Asistentes <strong>(socios)</strong>
                                                            <strong class="text-red">| $8,000 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Asistentes <strong>(no socios)</strong>
                                                            <strong class="text-red">| $10,500 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Estudiantes <strong>(presencial o zoom)</strong>
                                                            <strong class="text-red">| $750 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Asistente + curso <strong>(socios)</strong>
                                                            <strong class="text-red">| $13,500 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Asistente + curso <strong>(no socios)</strong>
                                                            <strong class="text-red">| $18,500 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Estudiante + curso <strong>(presencial o zoom)</strong>
                                                            <strong class="text-red">| $2,000 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Solo Curso <strong>(socios)</strong>
                                                            <strong class="text-red">| $8,000 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Solo Curso <strong>(no socios)</strong>
                                                            <strong class="text-red">| $10,500 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>Asistente <strong>ZOOM *</strong>
                                                            <strong class="text-red">| $7,500 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>Curso <strong>ZOOM *</strong>
                                                            <strong class="text-red">| $7,500 MXN</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>Asistente + Curso
                                                            <strong>ZOOM*</strong>
                                                            <strong class="text-red">| $10,500 MXN</strong>
                                                        </li>
                                                    </ul>
                                                    <a href="https://5congresoamitos.com.mx/registro/" target="_blank"
                                                        class="btn-custom text-white text-large">
                                                        ¡Regístrate aquí!
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- FIN - TABLA INSCRIPCIÓN NORMAL COSTOS 5CMITOS WEB 2023 -->

                                        <p class="text-red text-large">
                                            * Socios AMITOS y Sociedades Nacionales e Internaciones con convenio, 10% de
                                            descuento.
                                        </p>

                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </section>
            <!-- FIN - TABLAS COSTOS 5CMITOS WEB 2023 -->

            <!-- INICIO - PRECIO STAND 5CMITOS WEB 2023 -->
            <section id="call-to-action" class="bg-color call-to-action padding40" aria-label="cta">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-7">
                            <h3 class="text-dark size-2 no-margin text-white">
                                Notas
                                <br> a. Precios más impuestos (IVA)
                                <!-- <br> b. Fecha límite para incripción temprana, 02 de octubre 2023. -->
                                <br> b. Los socios considerados como tal son los
                                socios AMITOS, y los de las Sociedades
                                Nacionales e Internaciones con convenio.
                            </h3>
                        </div>
                    </div>
                </div>
            </section>
            <!-- FIN - PRECIO STAND 5CMITOS WEB 2023 -->
            <section>
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-7">
                            <h3 class="text-dark size-2 no-margin">
                                HOSPEDAJE | LODGMENT
                                <br>
                                <img src="img/costos/logo-radisson.jpg" alt="Logo Hotel Radisson Perisur">
                                <br>
                                Convenio con el Hotel Paraíso Radisson Perisur
                                <br>
                                Reservaciones directamente a través del correo:  reservaciones@radisson.com.mx
                                <br>
                                Código de reservación: <strong>“AMITOS2023”</strong>
                                <br>
                                At’n. Departamento de Reservaciones Radisson Paraíso Hotel Mexico 55 5927 5959
                                <br>
                                Dirección: Cúspide 53, Parques del Pedregal, Tlalpan, 14010. Ciudad de México, México
                            </h3>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- FIN - CONTENIDOS COSTOS 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("include/footer.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

</body>

</html>