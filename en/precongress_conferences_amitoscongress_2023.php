<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "Precongress Conferences | 5th Mexican Congress Of Tunnel Engineering and Underground Works | november - december 2023 | Mexico City";
$description = "Precongress Conferences. 5th Mexican Congress Of Tunnel Engineering and Underground Works. November 29<sup>th</sup> and 30<sup>th</sup>, December 1 <sup>st</sup>, 2023. Mexico City";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2020 -->
    <? include_once("../include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2020 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/en/index.php">
                                    <img class="logo" src="../img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2020 -->
                                <? include_once("../include/navbar_en.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2020 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contact
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - SUBHEADER CURSOS PRECONGRESO 5CMITOS WEB 2023 -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            PRECONGRESS CONFERENCES
                        </h1>
                        <ul class="crumb">
                            <li>
                                <a href="<?= $servidor ?>/en/index.php">
                                    Home
                                </a>
                            </li>
                            <li class="sep">
                                /
                            </li>
                            <li>
                                <a href="precongress_conferences_amitoscongress_2023.php">
                                    Precongress Conferences
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - SUBHEADER CURSOS PRECONGRESO 5CMITOS WEB 2023 -->

        <!-- INICIO - CONTENIDOS PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->
        <div id="content" class="no-bottom no-top">

            <section id="pricing-table">

                <div class="item pricing">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                                <h3>
                                    Program
                                </h3>
                                <div class="separator"><span><i class="fa fa-square"></i></span></div>
                            </div>
                        </div>

                        <div class="row">

                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-red text-center" width="200px">
                                            Day
                                        </th>
                                        <th scope="col" class="text-red text-center">
                                            Person
                                        </th>
                                        <th scope="col" class="text-red text-center">
                                            Subject
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            Tuesday October 3
                                            <br>15:00
                                        </th>
                                        <td class="text-center">
                                            Francisco A. Avila Aranda
                                            <br><strong>CEO Herrenknecht | Panama</strong>
                                        </td>
                                        <td class="text-center">
                                            General parameters for selecting 
                                            <br>and sizing tunnel boring machines.
                                            <br> <strong><a href="https://www.youtube.com/watch?v=B7VcrvGzB8E" target="_blank">View video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Tuesday October 10
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Manuel Saez Prieto
                                            <br><strong>Consultant and Co-Founder of Ágora Smart City | Mexico</strong>
                                        </td>
                                        <td class="text-center">
                                            How subway spaces accelerate the development of "Smart Cities", 
                                            <br>innovating and preserving the environment.
                                            <br> <strong><a href="https://www.youtube.com/watch?v=psOZRSmkRNo" target="_blank">View video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Tuesday 17 October
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Sergio Carmona Malatesta
                                            <br><strong>Universidad Técnica Federico Santa María | Chile</strong>
                                        </td>
                                        <td class="text-center">
                                            Testing of fiber-reinforced shotcrete to confirm performance during job execution.
                                            <br> <strong><a href="https://www.youtube.com/watch?v=IhnFM_X-86w" target="_blank">View video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Tuesday October 24	
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Carlos Mario Rosas Palomino
                                            <br><strong>Director of Electromechanical Equipment Inventory 
                                               <br>at the Toyo Tunnel - Colombia</strong>
                                        </td>
                                        <td class="text-center">
                                            Toyo Tunnel, the longest tunnel in America.
                                            <br> <strong><a href="https://www.youtube.com/watch?v=aLX2nKy3_AA" target="_blank">View video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Tuesday October 31	
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Jorge G. Laiun
                                            <br><strong>President of SRK Consulting | Argentina</strong>
                                        </td>
                                        <td class="text-center">
                                            Horizontal connection between deep tangent shafts of cast-in-wall.
                                            <br>Sewage pretreatment plant, Buenos Aires, Argentina
                                            <br> <strong><a href="https://www.youtube.com/watch?v=jG2pAdvUgyw" target="_blank">View video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Tuesday 7 November	
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Sina Moallemi
                                            <br><strong>Geomechanics Specialist and Lead Project
                                                <br>Manager at Rocscience | Canada </strong>
                                        </td>
                                        <td class="text-center">
                                            Streamlining 3D Finite Element 
                                            <br>Modelling for Tunnelling Applications
                                            <br> <strong><a href="https://www.youtube.com/watch?v=QJ51_Si49sg" target="_blank">View video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Tuesday, November 21	
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Israel Lagos
                                            <br><strong>UNAM Engineering Postgraduate | Mexico</strong>
                                        </td>
                                        <td class="text-center">
                                            Use of artificial intelligence for the estimation of operating parameters 
                                            <br>of a TBM by means of artificial neural networks
                                            <br> <strong><a href="https://www.youtube.com/watch?v=nvreKEsH_tk" target="_blank">View video</a></strong>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>

                        </div>
                        
                    </div>
                </div>
            </section>
            <center>
                <a href="../archivos/precongreso/programa-conferencias-precongreso.pdf"
                target="_blank" class="btn-custom text-white text-large">
                    Ver Flyer
                </a>
            </center>

        </div>
        <!-- FIN - CONTENIDOS PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("../include/footer_en.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("../include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

</body>

</html>