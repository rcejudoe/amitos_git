<!-- INICIO - FOOTER 5CMITOS 2023 -->
<footer class="light">
    <div class="container">
        <div class="row">
            <hr width="20px">
            <div class="col-lg-3">
                <img src="<?= $img ?>/logo/logo_40_amitos_sf.png" class="logo-small" alt="5 congreso amitos, túneles y obras subterráneas, cdmx, 2023">
                <br> 5 Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas.
            </div>

            <div class="col-lg-3">
                <div class="widget widget_recent_post">
                    <h3 class="text-blue-dark">
                        Congreso AMITOS 2023
                    </h3>
                    <ul>
                        <li>
                            <a href="<?= $servidor ?>/costos_inscripcion_congresoamitos_2023.php">
                                Costos
                            </a>
                        </li>
                        <li>
                            <a href="<?= $servidor ?>/expo_congresoamitos_2023.php">
                                Expo
                            </a>
                        </li>
                        <li>
                            <a href="<?= $servidor ?>/programa_tecnico_congresoamitos_2023.php">
                                Programa Técnico
                            </a>
                        </li>
                        <li>
                            <a href="<?= $servidor ?>/cursos_precongreso_congresoamitos_2023.php">
                                Cursos Precongreso
                            </a>
                        </li>
                        <li>
                            <a href="<?= $servidor ?>/comites_congresoamitos_2023.php">
                                Comités
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="widget widget_recent_post">
                    <h3 class="text-blue-dark">
                        Registro
                    </h3>
                    <ul>
                        <li>
                            <a href="https://5congresoamitos.com.mx/registro/" target="_blank">
                                <strong class="text-red">¡Regístrate aquí!</strong>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3">
                <h3 class="text-blue-dark">
                    Contacto
                </h3>
                <div class="widget widget-address">
                    <address>
                        <!--<span>
                            Camino a Santa Teresa 187 
                           <br> Col. Parques del Pedregal
                        </span>-->
                        <span>
                            <strong>Teléfono:</strong>
                            <a href="tel:+(52) 4579 8381">
                                (55) 4579-8381
                            </a>
                        </span>
                        <span>
                            <strong>Email:</strong>
                            <a href="mailto:amitos@amitos.org">
                                Contacto AMITOS
                            </a>
                        </span>
                        <span>
                            <strong>Web:</strong>
                            <a href="https://www.amitos.org/" target="blank">
                                AMITOS
                            </a>
                        </span>
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div class="subfooter">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    &copy; Todos los derechos reservados 2023.
                    <br> 5 Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas.
                    <br> Powered by <a href="https://www.puntozip.com.mx"><span class="id-color">Punto Zip.</span></a>
                </div>

                <div class="col-md-6 text-right text-large">
                    <div class="social-icons">
                        <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="blank">
                            <i class="fa fa-facebook fa-lg"></i>
                        </a>
                        <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="_blank">
                            <i class="fa fa-linkedin fa-lg"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <a href="#" id="back-to-top" class="light"></a>

</footer>
<!-- FIN - FOOTER 5CMITOS 2023 -->