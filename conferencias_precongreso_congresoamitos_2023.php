<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "Conferencias Precongreso | 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas | noviembre - diciembre 2023 | CDMX";
$description = "Conferencias Precongreso. 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas. 29 y 30 de noviembre, 01 de diciembre, 2023. CDMX";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2020 -->
    <? include_once("include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2020 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/index.php">
                                    <img class="logo" src="img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2020 -->
                                <? include_once("include/navbar.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2020 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contacto
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - SUBHEADER CURSOS PRECONGRESO 5CMITOS WEB 2023 -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            CONFERENCIAS PRECONGRESO
                        </h1>
                        <ul class="crumb">
                            <li>
                                <a href="<?= $servidor ?>/index.php">
                                    Inicio
                                </a>
                            </li>
                            <li class="sep">
                                /
                            </li>
                            <li>
                                <a href="conferencias_precongreso_congresoamitos_2023.php">
                                    Conferencias Precongreso
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - SUBHEADER CURSOS PRECONGRESO 5CMITOS WEB 2023 -->


        <!-- INICIO - CONTENIDOS PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->
        <div id="content" class="no-bottom no-top">

            <section id="pricing-table">

                <div class="item pricing">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                                <h3>
                                    Programa
                                </h3>
                                <div class="separator"><span><i class="fa fa-square"></i></span></div>
                            </div>
                        </div>

                        <div class="row">

                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-red text-center" width="200px">
                                            Día
                                        </th>
                                        <th scope="col" class="text-red text-center">
                                            Persona
                                        </th>
                                        <th scope="col" class="text-red text-center">
                                            Tema
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            Martes 3 Octubre
                                            <br>15:00
                                        </th>
                                        <td class="text-center">
                                            Francisco A. Avila Aranda
                                            <br><strong>CEO Herrenknecht | Panamá</strong>
                                        </td>
                                        <td class="text-center">
                                            Parámetros generales para seleccionar 
                                            <br> y dimensionar tuneladoras.
                                            <br> <strong><a href="https://www.youtube.com/watch?v=B7VcrvGzB8E" target="_blank">Ver video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Martes 10 Octubre
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Manuel Saez Prieto
                                            <br><strong>Consultor y Cofundador de Ágora Smart City | México</strong>
                                        </td>
                                        <td class="text-center">
                                            Cómo los espacios subterráneos aceleran el desarollo de "Smart Cities", 
                                            <br>innovando y preservando el medioambiente.
                                            <br> <strong><a href="https://www.youtube.com/watch?v=psOZRSmkRNo" target="_blank">Ver video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Martes 17 Octubre
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Sergio Carmona Malatesta
                                            <br><strong>Universidad Técnica Federico Santa María | Chile</strong>
                                        </td>
                                        <td class="text-center">
                                            Pruebas en concreto lanzado reforzado con fibras para la 
                                            <br>confirmación de su desempeño durante la ejecución de obra.
                                            <br> <strong><a href="https://www.youtube.com/watch?v=IhnFM_X-86w" target="_blank">Ver video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Martes 24 Octubre
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Carlos Mario Rosas Palomino
                                            <br><strong>Director de Interventoría de Equipos Electromecánicos 
                                               <br>en el Túnel del Toyo | Colombia</strong>
                                        </td>
                                        <td class="text-center">
                                            Túnel del Toyo, el túnel más largo de América.
                                            <br> <strong><a href="https://www.youtube.com/watch?v=aLX2nKy3_AA" target="_blank">Ver video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Martes 31 Octubre
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Jorge G. Laiun
                                            <br><strong>Presidente de SRK Consulting | Argentina</strong>
                                        </td>
                                        <td class="text-center">
                                            Conexión horizontal entre pozos tangentes profundos de muro colado.
                                            <br>Planta de pretratamiento de líquidos cloacales, Buenos Aires, Argentina
                                            <br> <strong><a href="https://www.youtube.com/watch?v=jG2pAdvUgyw" target="_blank">Ver video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Martes 7 Noviembre
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Sina Moallemi
                                            <br><strong>Especialista en Geomecánica y Líder de Proyecto 
                                                <br>Gerente en Rocscience | Canadá</strong>
                                        </td>
                                        <td class="text-center">
                                            Optimización del modelado de elementos 
                                            <br>finitos 3D para aplicaciones de túneles
                                            <br> <strong><a href="https://www.youtube.com/watch?v=QJ51_Si49sg" target="_blank">Ver video</a></strong>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th scope="row" class="text-center">
                                            Martes 21 Noviembre
                                            <br>17:00
                                        </th>
                                        <td class="text-center">
                                            Israel Lagos
                                            <br><strong>Posgrado de Ingeniería UNAM | México</strong>
                                        </td>
                                        <td class="text-center">
                                            Uso de la inteligencia artificial para la estimación de parámetros 
                                            <br> de operación de una TBM por medio de redes neuronales artificiales
                                            <br> <strong><a href="https://www.youtube.com/watch?v=nvreKEsH_tk" target="_blank">Ver video</a></strong>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>

                        </div>
                        
                    </div>
                </div>
            </section>
            <center>
                <a href="archivos/precongreso/programa-conferencias-precongreso.pdf"
                target="_blank" class="btn-custom text-white text-large">
                    Ver Flyer
                </a>
            </center>
            <!-- FIN - CONTENIDOS JUEVES 30 DE NOVIEMBRE 01 DICIEMBRE 5CMITOS WEB 2023 -->

        </div>
        <!-- FIN - CONTENIDOS PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("include/footer.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

</body>

</html>