<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "Organizing Committee | 5th Mexican Congress Of Tunnel Engineering and Underground Works | november - december 2023 | Mexico City";
$description = "Organizing Committee. 5th Mexican Congress Of Tunnel Engineering and Underground Works. November 29<sup>th</sup> and 30<sup>th</sup>, December 1 <sup>st</sup>, 2023. Mexico City";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2020 -->
    <? include_once("../include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2020 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/en/index.php">
                                    <img class="logo" src="../img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2020 -->
                                <? include_once("../include/navbar_en.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2020 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contact
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - SUBHEADER COMITÉS 5CMITOS WEB 2023 -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            Committees
                        </h1>
                        <ul class="crumb">
                            <li>
                                <a href="<?= $servidor ?>/en/index.php">
                                    Home
                                </a>
                            </li>
                            <li class="sep">
                                /
                            </li>
                            <li>
                                <a href="committees_amitoscongress_2023.php">
                                    Committees
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - SUBHEADER COMITÉS 5CMITOS WEB 2023 -->

        <!-- INICIO - CONTENIDOS COMITÉS 5CMITOS WEB 2023 -->
        <div id="content" class="no-bottom no-top">

            <!-- INICIO - COMITÉ ORGANIZADOR INFO CAMITOS 2023 -->
            <section data-bgcolor="#f9f9f9">
                <div class="container">
                    <div class="row">

                        <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                            <h3>
                                ORGANIZING COMMITTEE
                            </h3>
                            <div class="separator"><span><i class="fa fa-square"></i></span></div>
                        </div>

                        <div class="spacer-single"></div>

                        <!-- INICIO - FILA 01 COMITÉ ORGANIZADOR CAMITOS 2023 -->
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay="0s">
                            <div class="de-team-contact s2">
                                <img src="../img/comite_organizador_congreso_amitos_2023/roberto_gonzalez_ramirez_fino_comite_organizador_congresoamitos_2023.webp"
                                    class="img-responsive img-rounded" alt="" />
                                <h3>
                                    Roberto González Ramírez
                                </h3>
                                <div class="dtc-phone">
                                    Presidente
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".2s">
                            <div class="de-team-contact s2">
                                <img src="../img/comite_organizador_congreso_amitos_2023/jose_angel_castro_nieto_comite_organizador_congresoamitos_2023.webp"
                                    class="img-responsive img-rounded" alt="" />
                                <h3>
                                    José Ángel Castro Nieto
                                </h3>
                                <div class="dtc-phone">
                                    Tesorero
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".4s">
                            <div class="de-team-contact s2">
                                <img src="../img/comite_organizador_congreso_amitos_2023/alexandra_ossa_comite_organizador_congresoamitos_2023.webp" class="img-responsive img-rounded" alt="Alexandra Ossa López, comité organizador, 5 congreso amitos, cdmx, 2023" />
                                <h3>
                                    Alexandra Ossa López
                                </h3>
                                <div class="dtc-phone">
                                    Vicepresidenta Comité Científico
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".6s">
                            <div class="de-team-contact s2">
                                <img src="../img/comite_organizador_congreso_amitos_2023/andres_moreno_comite_organizador_congresoamitos_2023.webp" class="img-responsive img-rounded" alt="Andrés Moreno Fernández, comité organizador, 5 congreso amitos, cdmx, 2023" />
                                <h3>
                                    Andrés Moreno Fernández
                                </h3>
                                <div class="dtc-phone">
                                    Asesor
                                </div>
                            </div>
                        </div>
                        <!-- FIN - FILA 01 COMITÉ ORGANIZADOR CAMITOS 2023 -->
                        
                        <!-- INICIO - FILA 02 COMITÉ ORGANIZADOR CAMITOS 2023 -->
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay="0s">
                            <div class="de-team-contact s2">
                                <img src="../img/comite_organizador_congreso_amitos_2023/roberto_gonzalez_izquierdo_comite_organizador_congresoamitos_2023.webp" class="img-responsive img-rounded" alt="Roberto González Izquierdo, comité organizador, 5 congreso amitos, cdmx, 2023" />
                                <h3>
                                    Roberto González Izquierdo
                                </h3>
                                <div class="dtc-phone">
                                    Asesor
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".2s">
                            <div class="de-team-contact s2">
                                <img src="../img/comite_organizador_congreso_amitos_2023/jose_francisco_suarez_fino_comite_organizador_congresoamitos_2023.webp"
                                    class="img-responsive img-rounded" alt="" />
                                <h3>
                                    José Francisco Suárez Fino
                                    <br>
                                    (II UNAM)
                                </h3>
                                <div class="dtc-phone">
                                    Asesor
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".4s">
                            <div class="de-team-contact s2">
                                <img src="../img/comites_congreso_amitos_2023/hombre_presidente_sesion_congreso_amitos_2023.png"
                                    class="img-responsive img-rounded" alt="" />
                                <h3>
                                    Humberto Marengo Mogollón
                                </h3>
                                <div class="dtc-phone">
                                    Asesor
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".6s">
                            <div class="de-team-contact s2">
                                <img src="../img/comites_congreso_amitos_2023/hombre_presidente_sesion_congreso_amitos_2023.png"
                                    class="img-responsive img-rounded" alt="" />
                                <h3>
                                    Leonardo Guzmán León
                                </h3>
                                <div class="dtc-phone">
                                    Asesor
                                </div>
                            </div>
                        </div>
                        <!-- FIN - FILA 02 COMITÉ ORGANIZADOR CAMITOS 2023 -->
                        
                        <!-- INICIO - FILA 03 COMITÉ ORGANIZADOR CAMITOS 2023 -->
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay="0s">
                            <div class="de-team-contact s2">
                                <img src="../img/comites_congreso_amitos_2023/hombre_presidente_sesion_congreso_amitos_2023.png"
                                    class="img-responsive img-rounded" alt="" />
                                <h3>
                                    Raúl López Roldan
                                </h3>
                                <div class="dtc-phone">
                                    Asesor
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".2s">
                            <div class="de-team-contact s2">
                                <img src="../img/comite_organizador_congreso_amitos_2023/enrique_farjeat_paramo_comite_organizador_congresoamitos_2023.webp"
                                    class="img-responsive img-rounded" alt="" />
                                <h3>
                                    Enrique Farjeat Páramo
                                </h3>
                                <div class="dtc-phone">
                                    Asesor
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".4s">
                            <div class="de-team-contact s2">
                                <img src="../img/comite_organizador_congreso_amitos_2023/gabriel_ramirez_ordaz_comite_organizador_congresoamitos_2023.webp"
                                    class="img-responsive img-rounded" alt="" />
                                <h3>
                                    Gabriel Francisco Ramírez Ordaz
                                </h3>
                                <div class="dtc-phone">
                                    Asesor
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".6s">
                            <div class="de-team-contact s2">
                                <img src="../img/comite_organizador_congreso_amitos_2023/xvlll_consejo_directivo_amitos_comite_organizador_congresoamitos_2023.webp"
                                    class="img-responsive img-rounded" alt="" />
                                <h3>
                                    XVIII Consejo Directivo AMITOS
                                </h3>
                                <div class="dtc-phone">
                                    Asesor
                                </div>
                            </div>
                        </div>
                        <!-- FIN - FILA 03 COMITÉ ORGANIZADOR CAMITOS 2023 -->

                    </div>
                </div>
            </section>
            <!-- FIN - COMITÉ ORGANIZADOR INFO CAMITOS 2023 -->

        </div>
        <!-- FIN - CONTENIDOS COMITÉS 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("../include/footer_en.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("../include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

</body>

</html>