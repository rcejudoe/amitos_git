<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "Programa técnico | 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas | noviembre - diciembre 2023 | CDMX";
$description = "Programa técnico. 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas. 29 y 30 de noviembre, 01 de diciembre, 2023. CDMX";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2020 -->
    <? include_once("include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2020 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/index.php">
                                    <img class="logo" src="img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2020 -->
                                <? include_once("include/navbar.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2020 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contacto
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - SUBHEADER PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            PROGRAMA TÉCNICO
                        </h1>
                        <ul class="crumb">
                            <li>
                                <a href="<?= $servidor ?>/index.php">
                                    Inicio
                                </a>
                            </li>
                            <li class="sep">
                                /
                            </li>
                            <li>
                                <a href="programa_tecnico_congresoamitos_2023.php">
                                    Programa técnico
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - SUBHEADER PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->

        <!-- INICIO - CONTENIDOS PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->
        <div id="content" class="no-bottom no-top">

            <!-- INICIO - CONTENIDOS JUEVES 30 DE NOVIEMBRE 01 DICIEMBRE 5CMITOS WEB 2023 -->
            <section id="pricing-table">

                <div class="item pricing">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                                <h3>
                                    Jueves
                                    <br> 30 de noviembre de 2023
                                </h3>
                                <div class="separator"><span><i class="fa fa-square"></i></span></div>
                            </div>
                        </div>

                        <div class="row">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-red text-center" width="160px">
                                            Hora
                                        </th>
                                        <th scope="col" class="text-red text-center">

                                        </th>
                                        <!-- <th scope="col" class="text-red text-center">
                                            Organizador
                                        </th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            8:00 - 8:30
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Registro de asistentes | Café de bienvenida</strong>
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            8:30 - 9:00
                                        </th>
                                        <td class="text-center">
                                            <strong class="text-red">Inauguración</strong>
                                            <br>Sergio M. ALCOCER MARTÍNEZ DE CASTRO, Presidente AMITOS, 
                                            <br> Presidente Comité Organizador
                                        </td>
                                        <!-- <td class="text-center">
                                            Invitado especial, Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            9:00 - 9:45
                                        </th>
                                        <td class="text-center">
                                            Conferencia Magistral 1.1
                                            <br> <strong>Proceso integrado para la gestión de riesgos de proyectos de túneles</strong>
                                            <br>Andrés, MARULANDA ESCOBAR 
                                            <br> ITA, First Vice-President | Colombia
                                            <br> <strong>Organizador:</strong> Tecnología e innovación en la ingeniería de obras subterráneas
                                            <br>
                                            Presidente de Sesión Juan PAULÍN AGUIRRE
                                        </td>
                                        <!-- <td class="text-center">
                                            Juan Paulín Aguirre.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            9:45 - 10:30
                                        </th>
                                        <td class="text-center">
                                            Conferencia Magistral 1.2
                                            <br><strong>Implementación de métodos de perforación direccional para condiciones de terreno inestable y roca dura:
                                                <br>
                                                una solución costo-eficiente para geologías desafiantes en aplicaciones de excavación sin zanja</strong>
                                                <br>Roberto, ZILLANTE
                                                <br>
                                                PETRA, Chief Technology Officer | USA, Colombia
                                            <br> <strong>Organizador:</strong> Tecnología e innovación en la ingeniería de obras subterráneas
                                            <br>
                                            Presidente de Sesión Juan PAULÍN AGUIRRE
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Juan Paulín Aguirre.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            10:30 - 10:55
                                        </th>
                                        <td class="text-center">
                                            Panelista 1.1
                                            <br>
                                            <strong>Analítica avanzada aplicada a riesgos de excavación con una tuneladora EPB bajo altas presiones en cámara</strong>
                                            <br>
                                            Waldo, SALUD VELÁZQUEZ
                                            <br>
                                            SENER | México, España
                                            <br> <strong>Organizador:</strong> Tecnología e innovación en la ingeniería de obras subterráneas
                                            <br>
                                            Presidente de Sesión Juan PAULÍN AGUIRRE
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            10:55 - 11:20
                                        </th>
                                        <td class="text-center">
                                            Panelista 1.2
                                            <br>
                                            <strong>Avance continuo: desarrollos para una nueva tuneladora de Herrenknecht</strong>
                                            <br>
                                            Francisco A., AVILA ARANDA
                                            <br>
                                            Herrenknecht AG | Panamá
                                            <br> <strong>Organizador:</strong> Tecnología e innovación en la ingeniería de obras subterráneas
                                            <br>
                                            Presidente de Sesión Juan PAULÍN AGUIRRE

                                        </td>
                                        <!-- <td class="text-center">
                                            David Javier Juárez Flores.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            11:20 - 11:45
                                        </th>
                                        <td class="text-center">
                                            Panelista 1.3
                                            <br>
                                            <strong>Implementación de la metodología BIM en las obras subterráneas – experiencias recientes
                                            <br>
                                            en grandes proyectos de infraestructura</strong>
                                            <br>
                                            Eric, CARRERA
                                            <br>
                                            Lombardi SA | Suiza
                                            <br> <strong>Organizador:</strong> Tecnología e innovación en la ingeniería de obras subterráneas
                                            <br>
                                            Presidente de Sesión Juan PAULÍN AGUIRRE
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión David Javier Juárez Flores.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            11:45 - 12:05
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Coffee break | Expo y entrevistas</strong>
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            12:05 - 12:50
                                        </th>
                                        <td class="text-center">
                                            Conferencia Magistral 2.1
                                            <br>
                                            <strong>Industrias Peñoles -136 años de minería: retos y oportunidades para el desarrollo de la ingeniería nacional</strong>
                                            <br>
                                            Moises, DURÁN
                                            <br>
                                            Peñoles - Baluarte | México
                                            <br> <strong>Organizador: </strong> La minería y las obras subterráneas
                                            <br>
                                            Presidente de Sesión David Javier JUÁREZ FLORES
                                        </td>
                                        <!-- <td class="text-center">
                                            Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            12:50 - 13:35
                                        </th>
                                        <td class="text-center">
                                            Conferencia Magistral 2.2
                                            <br>
                                            <strong>Sistemas mecanizados para construcción y desarrollo de mineras subterráneas</strong>
                                            <br>
                                            Patrick, RENNKAMP
                                            <br>
                                            Herrenknecht AG, Chief Technology Officer | Alemania
                                            <br> <strong>Organizador: </strong> La minería y las obras subterráneas
                                            <br>
                                            Presidente de Sesión David Javier JUÁREZ FLORES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            13:35 - 14:20
                                        </th>
                                        <td class="text-center">
                                            Panelista 2.1
                                            <br>
                                            <strong>Técnicas emergentes en la construcción de tiros seguros, 
                                                <br> Cementation Américas futuro de la profundización de tiros
                                            </strong>
                                            <br>
                                            Shawn Eric, STICKLER | Armando, ARMENDARIZ
                                            <br>
                                            Cementation Mining | México
                                            <br> <strong>Organizador: </strong> La minería y las obras subterráneas
                                            <br>
                                            Presidente de Sesión David Javier JUÁREZ FLORES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            14:20 - 14:50
                                        </th>
                                        <td class="text-center">
                                            Panelista 2.2
                                            <br>
                                            <strong>Concretos de requerimientos especiales para obras mineras
                                            </strong>
                                            <br>
                                            Sergio Monserrat, OCAMPO TORRES
                                            <br>
                                            MAPEI | México
                                            <br> <strong>Organizador: </strong> La minería y las obras subterráneas
                                            <br>
                                            Presidente de Sesión David Javier JUÁREZ FLORES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            14:50 - 16:20
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Comida | Expo y entrevistas</strong>
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            16:20 - 17:25
                                        </th>
                                        <td class="text-center">
                                            Conferencia Magistral 3.1
                                            <br>
                                            <strong>The design of energy tunnels for sustainable cities
                                            </strong>
                                            <br>
                                            Marco, BARLA
                                            <br>
                                            Politecnico di Torino | Italia
                                            <br> <strong>Organizador: </strong> Métodos numéricos en obras subterráneas
                                            <br>
                                            Presidente de Sesión Miguel Ángel MÁNICA MALCOM
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            17:25 - 17:50
                                        </th>
                                        <td class="text-center">
                                            Panelista 3.1
                                            <br>
                                            <strong>Impacto del método de modelización y modelo constitutivo en el análisis de obras subterráneas en roca
                                            </strong>
                                            <br>
                                            Edgar, MONTIEL
                                            <br>
                                            SRK | Chile
                                            <br> <strong>Organizador: Métodos numéricos en obras subterráneas</strong>
                                            <br>
                                            Presidente de Sesión Miguel Ángel MÁNICA MALCOM
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            17:50 - 18:15
                                        </th>
                                        <td class="text-center">
                                            Panelista 3.2
                                            <br>
                                            <strong>Interacción sísmica entre estructuras elevadas y subterráneas en suelos blandos
                                            </strong>
                                            <br>
                                            Juan Manuel, MAYORAL VILLA
                                            <br>
                                            Instituto de Ingeniería, UNAM | México
                                            <br> <strong>Organizador: Métodos numéricos en obras subterráneas</strong>
                                            <br>
                                            Presidente de Sesión Miguel Ángel MÁNICA MALCOM
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            18:15 - 18:40
                                        </th>
                                        <td class="text-center">
                                            Panelista 3.3
                                            <br>
                                            <strong>Modelación numérica de excavaciones urbanas en suelos blandos
                                            </strong>
                                            <br>
                                            Felipe de Jesús, JIMÉNEZ RAMÍREZ
                                            <br>
                                            Posgrado de Ingeniería, UNAM | México
                                            <br> <strong>Organizador: Métodos numéricos en obras subterráneas</strong>
                                            <br>
                                            Presidente de Sesión Miguel Ángel MÁNICA MALCOM
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            18:40 - 19:05
                                        </th>
                                        <td class="text-center">
                                            Panelista 3.4
                                            <br>
                                            <strong>Evaluación numérica de la confiabilidad de un microtúnel en suelos blandos del valle de México
                                            </strong>
                                            <br>
                                            Valeria Laura, JÁQUEZ DOMÍNGUEZ
                                            <br>
                                            Instituto de Ingeniería, UNAM | México
                                            <br> <strong>Organizador: Métodos numéricos en obras subterráneas</strong>
                                            <br>
                                            Presidente de Sesión Miguel Ángel MÁNICA MALCOM
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                </tbody>
                            </table>





                            <div class="row">
                                <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                                    <h3>
                                        Viernes
                                        <br> 1 de diciembre de 2023
                                    </h3>
                                    <div class="separator"><span><i class="fa fa-square"></i></span></div>
                                </div>
                            </div>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-red text-center" width="160px">
                                            Hora
                                        </th>
                                        <th scope="col" class="text-red text-center">

                                        </th>
                                        <!-- <th scope="col" class="text-red text-center">
                                            Organizador
                                        </th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            8:00 - 8:30
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Café de bienvenida | Expo y entrevistas</strong>
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            8:30 - 9:15
                                        </th>
                                        <td class="text-center">
                                            Conferencia Magistral 4.1
                                            <br>
                                            <strong>La energía geotérmica y el mundo de los túneles</strong>
                                            <br>
                                            Carlos, LÓPEZ JIMENO 
                                            <br> Catedrático de la Universidad Politécnica de Madrid | España
                                            <br>
                                            <Strong>Organizador: </Strong>Geotermia y proyectos singulares en obras subterráneas
                                            <br>
                                            Presidente de Sesión José Francisco SUÁREZ FINO.
                                        </td>
                                        <!-- <td class="text-center">
                                            José Francisco Suárez Fino.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            9:15 - 10:00
                                        </th>
                                        <td class="text-center">
                                            Conferencia Magistral 4.2
                                            <br>
                                            <strong>Revestimiento de túneles en Escandinavia</strong>
                                            <br>
                                            Antonio, ALONSO JIMÉNEZ
                                            <br>
                                            CN Instituto Geológico y Minero de España, Consejo Superior de Investigaciones Científicas
                                            <br>
                                            <Strong>Organizador: </Strong>Geotermia y proyectos singulares en obras subterráneas
                                            <br>
                                            Presidente de Sesión José Francisco SUÁREZ FINO.

                                        </td>
                                        <!-- <td class="text-center">
                                            <strong>Conferencistas:</strong> Presidente de Sesión José Francisco Suárez
                                            Fino.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            10:00 - 10:25
                                        </th>
                                        <td class="text-center">
                                            Panelista 4.1
                                            <br>
                                            <strong>Nuevo enfoque para el diseño de túneles en zonas urbanas densamente pobladas</strong>
                                            <br>
                                            Juan Manuel, MAYORAL VILLA 
                                            <br> Instituto de Ingeniería, UNAM | México
                                            <br>
                                            <Strong>Organizador: </Strong>Geotermia y proyectos singulares en obras subterráneas
                                            <br>
                                            Presidente de Sesión José Francisco SUÁREZ FINO.
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            10:25 - 10:50
                                        </th>
                                        <td class="text-center">
                                            Panelista 4.2
                                            <br>
                                            <strong>Sistema de monitoreo geotécnico y estructural cámara de carga y túnel 
                                               <br> hidráulico en TBM – Riachuelo l3 – Buenos Aires</strong>
                                               <br>
                                               Carlo, VEZZOLI 
                                               <br>SISGEO LATINOAMÉRICA S.AS. | Colombia
                                               <br>
                                               <Strong>Organizador: </Strong>Geotermia y proyectos singulares en obras subterráneas
                                            <br>
                                            Presidente de Sesión José Francisco SUÁREZ FINO.
                                        </td>
                                        <!-- <td class="text-center">
                                            Raymundo Gerardo González Reyes.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            10:50 - 11:15
                                        </th>
                                        <td class="text-center">
                                            Panelista 4.3
                                            <br>
                                            <strong>Corte vs. Túnel, Túnel Falso</strong>
                                            <br>
                                            José Francisco, SUÁREZ FINO 
                                            <br> Consultec Ingeniería, Arquitectura y Supervisión | México
                                            <br>
                                               <Strong>Organizador: </Strong>Geotermia y proyectos singulares en obras subterráneas
                                            <br>
                                            Presidente de Sesión José Francisco SUÁREZ FINO.
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Raymundo Gerardo González Reyes.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            11:15 - 11:45
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Coffee break | Expo y entrevistas</strong>
                                        </td>
                                        <!-- <td class="text-center">
                                            Jardín del CICM, Jesús Díez Químico, Enólogo y Viticultor Mexicano | www.
                                            vinicultura.com.mx
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            11:45 - 12:30
                                        </th>
                                        <td class="text-center">
                                            Conferencia Magistral 5.1
                                            <br>
                                            <strong>Factores que definen la ejecución de un proyecto con tecnologías sin zanja</strong>
                                            <br>
                                            Luis Guillermo, MALDONADO 
                                            <br> LASTT, Expresidente | Colombia
                                            <br>
                                            <strong>Organizador: </strong>Microtúneles y la tecnología de las excavaciones sin zanja
                                            <br>
                                            Presidente de Sesión Raymundo Gerardo GONZÁLEZ REYES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            12:30 - 13:15
                                        </th>
                                        <td class="text-center">
                                            Conferencia Magistral 5.2
                                            <br>
                                            <strong>Túneles y microtúneles para mejorar el sistema de saneamiento en preparación 
                                              <br>  de los juegos olímpicos Paris 2024</strong>
                                            <br>
                                            Julien BRUNETON 
                                            <br> BESSAC Inc., Director | Francia
                                            <br>
                                            <strong>Organizador: </strong>Microtúneles y la tecnología de las excavaciones sin zanja
                                            <br>
                                            Presidente de Sesión Raymundo Gerardo GONZÁLEZ REYES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            13:15 - 13:40
                                        </th>
                                        <td class="text-center">
                                            Panelista 5.1
                                            <br>
                                            <strong>Instalación de accesos a la costa y tuberías submarinas 
                                               <br> con métodos sin zanja: tecnologías y estudios de casos prácticos</strong>
                                            <br>
                                            David, JUÁREZ 
                                            <br> Herrenknecht AG | México
                                            <br>
                                            <strong>Organizador: </strong>Microtúneles y la tecnología de las excavaciones sin zanja
                                            <br>
                                            Presidente de Sesión Raymundo Gerardo GONZÁLEZ REYES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            13:40 - 14:05
                                        </th>
                                        <td class="text-center">
                                            Panelista 5.2
                                            <br>
                                            <strong>La importancia de la herramienta de corte para la buena ejecución y construcción de túneles mecanizados</strong>
                                            <br>
                                            Donhe Joao, FUENTES 
                                            <br> Palmieri | México
                                            <br>
                                            <strong>Organizador: </strong>Microtúneles y la tecnología de las excavaciones sin zanja
                                            <br>
                                            Presidente de Sesión Raymundo Gerardo GONZÁLEZ REYES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            14:05 - 14:30
                                        </th>
                                        <td class="text-center">
                                            Panelista 5.3
                                            <br>
                                            <strong>La digitalización, automatización e inteligencia artificial el camino para reducir costes constructivos</strong>
                                            <br>
                                            Jonatan, RODRÍGUEZ MARCOS 
                                            <br> VMT GmbH (Gesellshaft für Vermessungtechnik) | España
                                            <br>
                                            <strong>Organizador: </strong>Microtúneles y la tecnología de las excavaciones sin zanja
                                            <br>
                                            Presidente de Sesión Raymundo Gerardo GONZÁLEZ REYES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            14:30 - 15:00
                                        </th>
                                        <td class="text-center">
                                            Video conmemorativo a los 40 años de AMITOS y homenaje a la ingeniería mexicana que ha escrito esta historia.
                                            <br>
                                            <strong>Organizador:</strong>
                                            Presidente AMITOS, Presidente Comité Organizador
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            15:00 - 17:30
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Comida de cierre y conferencia especial</strong>
                                            <br>
                                            Jardín del CICM, Jesús Díez Químico, Enólogo y Viticultor Mexicano | www.
                                            vinicultura.com.mx
                                        
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        
                    </div>
                </div>
            </section>
            <center>
                <a href="archivos/AMITOS_2023_plantilla_presentaciones.pptx"
                class="btn-custom text-white text-large">
                    Descargar plantilla para presentación
                </a>
            </center>
            <!-- FIN - CONTENIDOS JUEVES 30 DE NOVIEMBRE 01 DICIEMBRE 5CMITOS WEB 2023 -->

        </div>
        <!-- FIN - CONTENIDOS PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("include/footer.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

</body>

</html>