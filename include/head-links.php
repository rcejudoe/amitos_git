<?php

date_default_timezone_set('America/Mexico_City');
$raiz      = $_SERVER['DOCUMENT_ROOT'] . "/amitos/amitos_git";
$servidor  = "http://localhost/amitos/amitos_git";
$paginaPrincipal = "/amitos/amitos_git/index.php";

//PRODUCCION
//$servidor = "https://5congresoamitos.com.mx/";


$img = $servidor . "/img";
$css = $servidor . "/assets/css";
$js  = $servidor . "/assets/js";
$font  = $servidor . "/assets/fonts";
$revolution  = $servidor . "/assets/revolution";
$rsplugin  = $servidor . "/assets/rs-plugin";
$src  = $servidor . "/assets/src";


$menu01 = $menu02 = $menu03 = $menu04 =  $menu05 = "";
$url     = $_SERVER['PHP_SELF'];
$arrUrl  = explode("/", $url);
$nivel   = $arrUrl[count($arrUrl) - 2];
$current = end($arrUrl);
//echo "<br>" . $nivel . ":-:" . $current . "<br><br>";
//echo $url;

if ($nivel   == "inicio")                               $menu02 = "active";
elseif ($nivel   == "acerca de")                        $menu03 = "active";
elseif ($nivel   == "expo")                             $menu04 = "active";
elseif ($current == "programa")                         $menu05 = "active";
elseif ($current == "contacto")

?>

    <title>
        <?= $title ?>
    </title>

    <!-- FAVICON 5TO CONGRESO MEXICANO DE INGENIERÍA DE TÚNELES Y OBRAS SUBTERRÁNEAS 2023 -->
    <link href="<?= $img ?>/favicon/amitos_40_anios_favicon.png" rel="shortcut icon" type="image/x-icon">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="5to Congreso Mexicano de Ingeniería de Túneles y obras Subterráneas.">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <!-- INICIO - 5 CMITOS CSS FILES -->
    <link href="<?= $css ?>/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap" />
    <link href="<?= $css ?>/bootstrap-grid.min.css" rel="stylesheet" type="text/css" id="bootstrap-grid" />
    <link href="<?= $css ?>/bootstrap-reboot.min.css" rel="stylesheet" type="text/css" id="bootstrap-reboot" />
    <link href="<?= $css ?>/plugins.css" rel="stylesheet" type="text/css">
    <link href="<?= $css ?>/style.css" rel="stylesheet" type="text/css">
    <link href="<?= $css ?>/color.css" rel="stylesheet" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="<?= $css ?>/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="<?= $css ?>/colors/red.css" type="text/css" id="colors">

    <!-- RS5.0 Stylesheet -->
    <link rel="stylesheet" href="<?= $revolution ?>/css/settings.css" type="text/css">
    <link rel="stylesheet" href="<?= $revolution ?>/css/layers.css" type="text/css">
    <link rel="stylesheet" href="<?= $revolution ?>/css/navigation.css" type="text/css">
    <link rel="stylesheet" href="<?= $css ?>/rev-settings.css" type="text/css">

    <!-- custom style -->
    <link rel="stylesheet" href="<?= $css ?>/custom-painting.css" type="text/css">
    <link rel="stylesheet" href="<?= $css ?>/custom.css" type="text/css">