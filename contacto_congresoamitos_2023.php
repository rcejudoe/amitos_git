<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "Contacto | 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas | noviembre - diciembre 2023 | CDMX";
$description = "Contacto. 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas. 29 y 30 de noviembre, 01 de diciembre, 2023. CDMX";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2020 -->
    <? include_once("include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2020 -->
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    <script src='https://www.google.com/recaptcha/api.js?render=6LdAH8IlAAAAAE_yKpDR3waHwu9I4XPweCFRLyJC'></script>
</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/index.php">
                                    <img class="logo" src="img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2020 -->
                                <? include_once("include/navbar.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2020 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contacto
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - SUBHEADER CONTACTO 5CMITOS WEB 2023 -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            Contacto
                        </h1>
                        <ul class="crumb">
                            <li>
                                <a href="<?= $servidor ?>/index.php">
                                    Inicio
                                </a>
                            </li>
                            <li class="sep">
                                /
                            </li>
                            <li>
                                <a href="contacto_congresoamitos_2023.php">
                                    Contacto
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - SUBHEADER CONTACTO 5CMITOS WEB 2023 -->

        <!-- INICIO - GOOGLE MAPS FORM CONTACTO 5CMITOS WEB 2023 -->
        <div id="content" class="no-top">

            <section id="de-map" class="no-top" aria-label="map-container">
                <div class="map-container map-fullwidth">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3765.662195476092!2d-99.19337988509542!3d19.297050986961462!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cdffd63c50131b%3A0xb1d0081cde6442cf!2sColegio%20de%20Ingenieros%20Civiles%20de%20M%C3%A9xico%2C%20A.C.!5e0!3m2!1ses-419!2smx!4v1682469986902!5m2!1ses-419!2smx" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </section>

            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <form action="envio_formulario.php" method="post">
                            <div class="row">
                                <div class="col-md-12 mb10">
                                    <h3>
                                        Escríbenos con cualquier duda o comentario.
                                    </h3>
                                </div>

                                <div class="col-md-6">

                                    <div>
                                        <input type='text' name='nombre' id='name' class="form-control" placeholder="Nombre completo" required>
                                    </div>

                                    <div>
                                        <input type='email' name='correo' id='email' class="form-control mt20" placeholder="Correo electrónico" required>
                                    </div>

                                    <div>
                                        <input type='text' name='telefono' id='phone' class="form-control mt20" placeholder="Teléfono" required>
                                    </div>
                                </div>

                                <div class="col-md-6">

                                    <div>
                                        <textarea name='mensaje' id='message' class="form-control" placeholder="Tu mensaje aquí" required></textarea>
                                    </div>
                                </div>

                                <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response" /><br>

                                <div class="col-md-12">

                                    <p id='submit' class="mt20">
                                        <input type='submit' id='send_message' value='Enviar' class="btn btn-line">
                                    </p>
                                </div>

                            </div>
                        </form>

                        <div id="success_message" class='success'>
                            Tu mensaje ha sido enviado con éxito. Recarga esta página para enviar otros mensajes.
                        </div>

                        <div id="error_message" class='error'>
                            ¡Lo siento! Tu mensaje no se ha enviado de manera correcta, vuelve a intentarlo.
                        </div>

                    </div>


                </div>
            </div>
        </div>
        <!-- FIN - GOOGLE MAPS FORM CONTACTO 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("include/footer.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>


    <!-- SCRIPT CAPTCHA -->
    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6LdAH8IlAAAAAE_yKpDR3waHwu9I4XPweCFRLyJC', {
                    action: 'homepage'
                })
                .then(function(token) {
                    //console.log(token);
                    document.getElementById('g-recaptcha-response').value = token;
                });
        });
    </script>

    <script>
        if (location.href.includes('registro=1')) {
            Swal.fire({
                position: 'top-center',
                icon: 'success',
                title: '¡Gracias por tu mensaje!, nosotros nos podremos en contacto contigo.',
                showConfirmButton: false,
                timer: 2400
            })
        }
    </script>

</body>

</html>