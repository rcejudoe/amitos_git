<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas | noviembre - diciembre 2023 | CDMX";
$description = "5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas. 29 y 30 de noviembre, 01 de diciembre, 2023. CDMX";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2023 -->
    <? include_once("include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2023 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/index.php">
                                    <img class="logo" src="img/logo/logo_40_amitos_sf_2.webp" alt="5 congreso amitos, logo, 2023, cdmx">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2023 -->
                                <? include_once("include/navbar.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2023 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contacto
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - CONTENIDOS INDEX 5CMITOS WEB 2023 -->
        <div id="content" class="no-bottom no-top">

            <!-- INICIO - PRESENTACION 5CMITOS WEB 2023 -->
            <section id="section-hero" class="vertical-center jarallax text-light" aria-label="section">
                <img src="img/index/amitos_congreso_2023.webp" class="jarallax-img" alt="5 congreso amitos, bellas artes, cdmx, 2023">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="spacer-single"></div>
                        <div class="col-lg-5 mb-sm-30">
                            <h2 class="style-5">
                                <br> 29 y 30 de noviembre
                                <br> 1 de diciembre
                                <br> 2023
                            </h2>
                            <h3>
                                Sede
                            </h3>
                            <p class="text-large text-white">
                                Colegio de Ingenieros Civiles de México, Camino Santa Teresa 187, Parques del Pedregal,
                                Tlalpan, C.P. 14010, Ciudad de México, México.
                            </p>
                            <!--<a href="#section-features" class="btn-custom text-light">
									Our Services
								</a>-->
                            <div class="spacer-single"></div>
                        </div>

                        <div class="col-lg-6 offset-lg-1 text-middle">
                            <img src="img/logo/logo_5cmitos_g.webp" alt="logo, 5 congreso amitos, cdmx, 2023" class="img-responsive" />
                        </div>
                    </div>
                    <div class="spacer-single"></div>
                </div>
            </section>
            <!-- FIN - PRESENTACION 5CMITOS WEB 2023 -->

            <!-- INICIO - COLUMNAS IMG INFO GENERAL 5CMITOS WEB 2023 -->
            <section id="section-services" class="no-top no-bottom  mt-50">
                <div class="container">
                    <div class="row g-0">

                        <div class="col-lg-3 sm-mb-30 wow fadeInUp" data-wow-delay="0s">

                            <img src="img/index/amitos_congreso_img_ch_01.webp" class="img-responsive" alt="5 congreso amitos, museo soumaya, cdmx, 2023">

                            <div class="text padding30" data-bgcolor="#f2f2f2">
                                <h3>
                                    29 de noviembre
                                </h3>
                                <p class="mb10">
                                    <i>Cursos precongreso</i><br>
                                    <br> - Túneles convencionales y sus obras complementarias, planeación y
                                    construcción.
                                    <br> - Túneles mecanizados, microtúneles y sus pozos de acceso, planeación y
                                    construcción.
                                    <br> - Análisis numérico de obras subterráneas.
                                    <br> - Instrumentación de obras subterráneas.
                                </p>
                            </div>

                        </div>

                        <div class="col-lg-3 sm-mb-30 wow fadeInUp" data-wow-delay=".3s">

                            <div class="text padding30" data-bgcolor="#f6f6f6">
                                <h3>
                                    30 de noviembre y 1 de diciembre
                                </h3>
                                <p class="mb10">
                                    <i>Sesiones técnicas</i>
                                    <br> - Tecnología e innovación en la ingeniería de obras subterráneas.
                                    <br> - La minería y las obras subterráneas.
                                    <br> - Métodos numéricos en obras subterráneas.
                                    <br> - Geotermia y proyectos singulares en obras subterráneas.
                                    <br> - Microtúneles y la tecnología de las excavaciones sin zanja.
                                </p>
                            </div>

                            <img src="img/index/amitos_congreso_img_ch_02.webp" class="img-responsive" alt="5 congreso amitos, ángel independencia, cdmx, 2023">

                        </div>

                        <div class="col-lg-3 sm-mb-30 wow fadeInUp" data-wow-delay=".6s">

                            <img src="img/index/amitos_congreso_img_ch_03.webp" class="img-responsive" alt="5 congreso amitos, zócalo capitalino, cdmx, 2023">

                            <div class="text padding30" data-bgcolor="#f2f2f2">
                                <h3>
                                    Fecha límite de envío de resúmenes para las sesiones técnicas:
                                </h3>
                                <p class="mb10">
                                    Viernes 9 de junio de 2023. <br>
                                    <br> Los resúmenes deberán enviarse al correo:
                                    <br><br> <strong>amitos@amitos.org</strong>
                                    <br><br> El formato será hibrido (presencial y a distancia).
                                </p>
                            </div>

                        </div>

                        <div class="col-lg-3 sm-mb-30 wow fadeInUp" data-wow-delay=".9s">

                            <div class="text padding30" data-bgcolor="#f6f6f6">
                                <p class="mb10">
                                    Este XVIII Consejo Directivo les extiende una cordial invitación a sumarse a este
                                    importante evento.
                                    <br><br> ¡Aparten las fechas!
                                    <br><br> <strong>Proyectemos la imagen de nuestro futuro y hagamos un túnel para llegar a ella.</strong>
                                </p>
                            </div>

                            <img src="img/index/amitos_congreso_img_ch_04.webp" class="img-responsive" alt="5 congreso amitos, mitikah torre, cdmx, 2023">

                        </div>

                    </div>
                </div>
            </section>
            <!-- FIN - COLUMNAS IMG INFO GENERAL 5CMITOS WEB 2023 -->

            <!-- INICIO - CALENDARIO DEL EVENTO 5CMITOS WEB 2023 -->
            <section class="de_light bg-white">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12 text-center">
                            <h2>
                                Calendario del evento
                            </h2>
                        </div>

                        <div class="col-md-6 offset-md-3">
                            <div class="timeline exp">

                                <div class="tl-block wow fadeInUp" data-wow-delay="0">
                                    <div class="tl-time">
                                        <h4>
                                            9 de junio
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Fecha límite para el envío de resúmenes.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".3s">
                                    <div class="tl-time">
                                        <h4>
                                            16 de junio
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Fecha límite de notificación de aceptación de resúmenes.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".6s">
                                    <div class="tl-time">
                                        <h4>
                                            15 de agosto
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Fecha límite para el envío de artículos.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".9s">
                                    <div class="tl-time">
                                        <h4>
                                            22 de agosto
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Fecha límite de notificación de aceptación de artículos.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".12s">
                                    <div class="tl-time">
                                        <h4>
                                            2 de octubre
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Fecha límite de notificación de presentación oral.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".15s">
                                    <div class="tl-time">
                                        <h4>
                                            1 de noviembre
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Fecha límite de inscripción de personas que presentarán trabajos
                                                presencialmente.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <hr>

                    </div>
                </div>
            </section>
            <!-- FIN - CALENDARIO DEL EVENTO 5CMITOS WEB 2023 -->

            <!-- INICIO - CALENDARIO INTERNO CURSOS SESIONES 5CMITOS WEB 2023
            <section class="de_light bg-white">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12 text-center">
                            <h2>
                                Calendario Interno, Cursos y Sesiones Técnicas.
                            </h2>
                        </div>

                        <div class="col-md-6 offset-md-3">
                            <div class="timeline exp">

                                <div class="tl-block wow fadeInUp" data-wow-delay="0">
                                    <div class="tl-time">
                                        <h4>
                                            23 de junio
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Fecha límite para presentar propuestas de conferencista
                                                magistral.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                                <div class="tl-block wow fadeInUp" data-wow-delay=".3s">
                                    <div class="tl-time">
                                        <h4>
                                            23 de junio
                                        </h4>
                                    </div>
                                    <div class="tl-bar">
                                        <div class="tl-line"></div>
                                    </div>
                                    <div class="tl-message">
                                        <div class="tl-icon">&nbsp;</div>
                                        <div class="tl-main text-large text-red">
                                            <strong>Fecha límite para presentar temario de cursos y nombres de
                                                conferencistas magistrales.</strong>
                                            <br><br>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <hr>

                    </div>
                </div>
            </section>
            FIN - CALENDARIO INTERNO CURSOS SESIONES 5CMITOS WEB 2023 -->

            <!-- INICIO - INFO INTRO 5CMITOS WEB 2023 -->
            <section id="section-text">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-4 offset-md-1 sm-mb-30 text-center wow fadeInRight">
                            <div class="de-images">
                                <img class="di-small wow fadeIn" src="img/index/amitos_congreso_img_ch_05.webp" alt="5 congreso amitos, túneles y obras subterráneas, cdmx, 2023" />
                                <img class="di-small-2" src="img/index/amitos_congreso_img_ch_07.webp" alt="5 congreso amitos, túneles y obras subterráneas, cdmx, 2023" />
                                <img class="img-fluid wow fadeInRight" data-wow-delay=".25s" src="img/index/amitos_congreso_img_ch_06.webp" alt="5 congreso amitos, túneles y obras subterráneas, cdmx, 2023" />
                            </div>
                        </div>

                        <div class="col-lg-5 offset-md-1 wow fadeInLeft" data-wow-delay="0s">
                            <h3 class="mb20">
                                Este 2023, la Asociación Mexicana de Ingeniería de Túneles y Obras
                                Subterráneas, también
                                conocida como AMITOS, cumple 40 años.
                            </h3>
                            <p>
                                Fue fundada el 13 de junio de 1983. El Quinto Congreso Mexicano de AMITOS será la
                                ocasión de <strong>conmemorar estos 40 años</strong> de desarrollo de la ingeniería de
                                túneles y obras subterráneas mexicana, así como
                                <strong>homenajear a quienes han dejado huella en esta historia</strong>.
                                <br><br> Con sede en la CDMX, <strong>constará de cuatro cursos precongreso, cinco
                                    sesiones técnicas, así como 10 conferencias magistrales con ponentes nacionales e
                                    internacionales</strong>. Además, contaremos con una
                                zona comercial para expositores. El evento concluirá con la participación de un invitado
                                especial, quien nos hablará sobre temas culturales ajenos y no tan ajenos a la
                                ingeniería.
                                <br><br> Esperamos contar con una participación importante de las nuevas generaciones de
                                ingenieros.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- FIN - INFO INTRO 5CMITOS WEB 2023 -->

            <!-- INICIO - 40 AÑOS DE AMITOS INFO 5CMITOS WEB 2023 -->
            <section id="section-action" class="jarallax text-light" aria-label="cta">
                <img src="img/index/amitos_congreso_2023_2.webp" class="jarallax-img" alt="5 congreso amitos, túneles y obras subterráneas, 40 años, espacio subterráneo, cdmx, 2023">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-4 wow fadeInUp" data-wow-delay=".2s">
                            <div class="de_count ultra-big text-center">
                                <h3 class="timer" data-to="40" data-speed="3000">
                                    1
                                </h3>
                                <span class="text-white">
                                    Años Desarrollando el espacio subterráneo
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-4 wow fadeInUp">
                            <h2>
                                Asociación Mexicana de Ingeniería de Túneles y Obras Subterráneas.
                            </h2>
                        </div>
                        <div class="col-lg-4 wow fadeInUp">
                            <p class="text-large">
                                ¡No olvides! realizar tu registro para el
                                <br> 5 Congreso de AMITOS 2023.
                            </p>
                            <a href="https://5congresoamitos.com.mx/registro/" target="blank" class="btn-custom text-light wow fadeInUp">
                                ¡Regístrate aquí!
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- FIN - 40 AÑOS DE AMITOS INFO 5CMITOS WEB 2023 -->

            <!-- INICIO - ICONOS NAVEGACIÓN INFO 5CMITOS WEB 2023 -->
            <section id="section-text-2">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-4 wow fadeIn" data-wow-delay="0s">
                            <div class="box-number square sm-mb-30">
                                <i class="icon_documents bg-color text-white"></i>
                                <div class="text">
                                    <h3>
                                        Programa técnico
                                    </h3>
                                    <p>
                                        Conoce la serie de actividades a realizarse en el Congreso de AMITOS 2023.
                                    </p>
                                    <a href="programa_tecnico_congresoamitos_2023.php" class="btn-custom text-light wow fadeInUp">
                                        ¡Conoce más!
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 wow fadeIn" data-wow-delay=".5s">
                            <div class="box-number square sm-mb-30">
                                <i class="icon_mic bg-color text-white"></i>
                                <div class="text">
                                    <h3>
                                        Expo
                                    </h3>
                                    <p>
                                        Te invitamos a conocer los stands de los participantes en el congreso.
                                    </p>
                                    <a href="expo_congresoamitos_2023.php" class="btn-custom text-light wow fadeInUp">
                                        ¡Conoce más!
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 wow fadeIn" data-wow-delay="1s">
                            <div class="box-number square sm-mb-30">
                                <i class="icon_creditcard bg-color text-white"></i>
                                <div class="text">
                                    <h3>
                                        Costos
                                    </h3>
                                    <p>
                                        ¡Ya conoces los costos para el 5 Congreso de AMITOS 2023!
                                    </p>
                                    <a href="costos_inscripcion_congresoamitos_2023.php" class="btn-custom text-light wow fadeInUp">
                                        ¡Conoce más!
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- FIN - ICONOS NAVEGACIÓN INFO 5CMITOS WEB 2023 -->

            <!-- INICIO - PATROCINIOS 5CMITOS WEB 2023 -->
            <?php include_once("include/patrocinadores_congresoamitos_2023.php"); ?>
            <!-- FIN - PATROCINIOS 5CMITOS WEB 2023 -->

        </div>
        <!-- FIN - CONTENIDOS INDEX 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("include/footer.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

    <!-- INICIO - JS AUTOPLAY PATROCINADORES 5CMITOS 2023 -->
    <script>
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            items: 6,
            loop: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true
        });
    </script>
    <!-- FIN - JS AUTOPLAY PATROCINADORES 5CMITOS 2023 -->

</body>

</html>