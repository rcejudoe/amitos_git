<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "Presidentes de Sesión | 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas | noviembre - diciembre 2023 | CDMX";
$description = "Presidentes de Sesión. 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas. 29 y 30 de noviembre, 01 de diciembre, 2023. CDMX";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2020 -->
    <? include_once("include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2020 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/index.php">
                                    <img class="logo" src="img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2020 -->
                                <? include_once("include/navbar.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2020 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contacto
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - SUBHEADER PRESIDENTES SESIÓN 5CMITOS WEB 2023 -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            PRESIDENTES DE SESIÓN
                        </h1>
                        <ul class="crumb">
                            <li>
                                <a href="<?= $servidor ?>/index.php">
                                    Inicio
                                </a>
                            </li>
                            <li class="sep">
                                /
                            </li>
                            <li>
                                <a href="presidentes_sesion_congresoamitos_2023.php">
                                    Presidentes de sesión
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - SUBHEADER PRESIDENTES SESIÓN 5CMITOS WEB 2023 -->

        <!-- INICIO - CONTENIDOS COMITÉS 5CMITOS WEB 2023 -->
        <div id="content" class="no-bottom no-top">

            <!-- INICIO - PRESIDENTES SESIÓN INFO CAMITOS 2023 -->
            <section data-bgcolor="#f9f9f9">
                <div class="container">
                    <div class="row">

                        <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                            <h3>
                                PRESIDENTES DE SESIÓN
                            </h3>
                            <div class="separator"><span><i class="fa fa-square"></i></span></div>
                        </div>

                        <div class="spacer-single"></div>

                        <!-- INICIO - FILA 01 PRESIDENTES SESIONES CAMITOS 2023 -->
                        <div class="col-md-4 wow fadeInUp" data-wow-delay="0s">
                            <div class="de-team-contact s2">
                                <img src="img/presidentes_sesion_congreso_amitos_2023/juan_paulin_aguirre_presidente_sesion_congresoamitos_2023.webp" class="img-responsive img-rounded" alt="Juan Paulin Aguirre, Presidentes de sesión, 5 Congreso AMITOS, CDMX, 2023" />
                                <h3>
                                    Juan Paulin Aguirre
                                </h3>
                                <div class="dtc-phone">
                                    Presidente AMITOS
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 wow fadeInUp" data-wow-delay=".2s">
                            <div class="de-team-contact s2">
                                <img src="img/presidentes_sesion_congreso_amitos_2023/david_juarez_comite_organizador_congresoamitos_2023.webp" class="img-responsive img-rounded" alt="David Javier Juárez Flores, Presidentes de sesión, 5 Congreso AMITOS, CDMX, 2023" />
                                <h3>
                                    David Javier Juárez Flores
                                </h3>
                                <div class="dtc-phone">
                                    Herrenknecht AG
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 wow fadeInUp" data-wow-delay=".4s">
                            <div class="de-team-contact s2">
                                <img src="img/presidentes_sesion_congreso_amitos_2023/miguel_angel_manica_malcom_presidente_sesion_congresoamitos_2023.webp" class="img-responsive img-rounded" alt="Miguel Ángle Mánica Malcom, Presidentes de sesión, 5 Congreso AMITOS, CDMX, 2023" />
                                <h3>
                                    Miguel Ángel Mánica Malcom
                                </h3>
                                <div class="dtc-phone">
                                    II UNAM
                                </div>
                            </div>
                        </div>
                        <!-- FIN - FILA 01 PRESIDENTES SESIONES CAMITOS 2023 -->

                        <!-- INICIO - FILA 02 COMITÉ ORGANIZADOR CAMITOS 2023 -->
                        <div class="col-md-4 wow fadeInUp" data-wow-delay=".6s">
                            <div class="de-team-contact s2">
                                <img src="img/presidentes_sesion_congreso_amitos_2023/jose_francisco_suarez_fino_presidente_sesion_congresoamitos_2023.webp" class="img-responsive img-rounded" alt="José Francisco Suárez Fino, Presidentes de sesión, 5 Congreso AMITOS, CDMX, 2023" />
                                <h3>
                                    José Francisco Suárez Fino
                                </h3>
                                <div class="dtc-phone">
                                    Consultec Ingeniería
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 wow fadeInUp" data-wow-delay="0s">
                            <div class="de-team-contact s2">
                                <img src="img/presidentes_sesion_congreso_amitos_2023/raymundo_gerardo_gonzalez_reyes_presidente_sesion_congresoamitos_2023.webp" class="img-responsive img-rounded" alt="Raymundo Gerardo González Reyes, Presidentes de sesión, 5 Congreso AMITOS, CDMX, 2023" />
                                <h3>
                                    Raymundo Gerardo González Reyes
                                </h3>
                                <div class="dtc-phone">
                                    BESSAC
                                </div>
                            </div>
                        </div>
                        <!-- FIN - FILA 02 COMITÉ ORGANIZADOR CAMITOS 2023 -->

                    </div>
                </div>
            </section>
            <!-- FIN - PRESIDENTES SESIÓN INFO CAMITOS 2023 -->

        </div>
        <!-- FIN - CONTENIDOS COMITÉS 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("include/footer.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

</body>

</html>