<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "Cursos Precongreso | 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas | noviembre - diciembre 2023 | CDMX";
$description = "Cursos Precongreso. 5to Congreso Mexicano de Ingeniería de Túneles y Obras Subterráneas. 29 y 30 de noviembre, 01 de diciembre, 2023. CDMX";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2020 -->
    <? include_once("include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2020 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/index.php">
                                    <img class="logo" src="img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2020 -->
                                <? include_once("include/navbar.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2020 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contacto
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - SUBHEADER CURSOS PRECONGRESO 5CMITOS WEB 2023 -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            CURSOS PRECONGRESO
                        </h1>
                        <ul class="crumb">
                            <li>
                                <a href="<?= $servidor ?>/index.php">
                                    Inicio
                                </a>
                            </li>
                            <li class="sep">
                                /
                            </li>
                            <li>
                                <a href="cursos_precongreso_congresoamitos_2023.php">
                                    Cursos Precongreso
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - SUBHEADER CURSOS PRECONGRESO 5CMITOS WEB 2023 -->

        <!-- INICIO - CONTENIDOS CURSOS PRECONGRESO 5CMITOS WEB 2023 -->
        <div id="content" class="no-bottom no-top">

            <!-- INICIO - CONTENIDOS MIERCOLES 29 DE NOVIEMBRE 5CMITOS WEB 2023 -->
            <section id="pricing-table">

                <div class="item pricing">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                                <h3>
                                    Miércoles
                                    <br> 29 de noviembre de 2023
                                </h3>
                                <div class="separator"><span><i class="fa fa-square"></i></span></div>
                            </div>
                        </div>

                        <div class="row">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-red text-center">
                                            Horario de los cursos
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            8:00 - 8:30
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Inauguración y registro de asistentes.</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            8:30 - 10:45
                                        </th>
                                        <td class="text-center">
                                            <strong>Sesión 1</strong>
                                            <!--| Túneles convencionales y sus obras complementarias, planeación y construcción.-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            10:45 - 11:00
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Coffee break</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            11:00 - 13:15
                                        </th>
                                        <td class="text-center">
                                            <strong>Sesión 2</strong>
                                            <!--| Túneles mecanizados, microtúneles, y sus pozos de acceso, planeación y construcción.-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            13:15 - 14:45
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Comida</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            14:45 - 17:00
                                        </th>
                                        <td class="text-center">
                                            <strong>Sesión 3</strong>
                                            <!--| Análisis numérico de obras subterráneas. **-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            17:00 - 17:15
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Coffee break</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            17:15 - 18:30
                                        </th>
                                        <td class="text-center">
                                            <strong>Sesión 4</strong> | <strong class="text-red">Clausura</strong>
                                            <!-- | Instrumentación de obras subterráneas. |-->
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <p class="text-red">
                                ** Los asistentes deberían llevar sus computadoras, quienes así lo deseen
                            </p>

                        </div>

                        <div class="row">

                            <div class="col-md-3">
                                <img src="images-industry/services/1.jpg" class="img-responsive" alt="">
                                <div class="spacer-single"></div>
                                <strong class="text-red">TÚNELES CONVENCIONALES Y SUS OBRAS COMPLEMENTARIAS, PLANEACIÓN
                                    Y CONSTRUCCIÓN.</strong>
                                <br> Auditorio Manuel Anaya | 65 px
                                <br><br> Coordinador:
                                <br> Carlos Alfonso Ramírez de Arellano
                                <br>
                                <a href="https://www.knightpiesold.com/en/" target="blank">
                                    <strong class="text-yellow">Knight Piésold Consulting</strong>
                                </a>
                                <br><br>
                                <a href="archivos/precongreso/programa_1ºcurso_precongreso_v2.pdf" target="_blank"
                                    class="btn-custom text-white text-large">
                                    Programa
                                </a>
                            </div>

                            <div class="col-md-3">
                                <img src="images-industry/services/1.jpg" class="img-responsive" alt="">
                                <div class="spacer-single"></div>
                                <strong class="text-red">TÚNELES MECANIZADOS, MICROTÚNELES, Y SUS POZOS DE ACCESO,
                                    PLANEACIÓN Y CONSTRUCCIÓN.</strong>
                                <br> Aula Leopoldo Lieberman "A" | 24 px
                                <br><br> Coordinador:
                                <br> Roberto González Ramírez
                                <br>
                                <a href="https://www.moldequipo.com/" target="blank">
                                    <strong class="text-yellow">Moldequipo</strong>
                                </a>
                                <br><br>
                                <a href="archivos/precongreso/programa_2ºcurso_precongreso_v2.pdf" target="_blank"
                                    class="btn-custom text-white text-large">
                                    Programa
                                </a>
                            </div>

                            <div class="col-md-3">
                                <img src="images-industry/services/1.jpg" class="img-responsive" alt="">
                                <div class="spacer-single"></div>
                                <strong class="text-red">ANÁLISIS NUMÉRICO DE OBRAS SUBTERRÁNEAS.**</strong>
                                <br> Aula Leopoldo Lieberman "B" | 24 px
                                <br><br> Coordinador:
                                <br> Felipe De Jesús Jiménez Ramírez
                                <br>
                                <a href="http://www.iingen.unam.mx/es-mx/Paginas/Splash/Default.aspx" target="blank">
                                    <strong class="text-yellow">II UNAM</strong>
                                </a>
                                <br>
                                <br>
                                <a href="archivos/precongreso/programa_3ºcurso_precongreso.pdf" target="_blank"
                                    class="btn-custom text-white text-large">
                                    Programa
                                </a>

                            </div>

                            <div class="col-md-3">
                                <img src="images-industry/services/1.jpg" class="img-responsive" alt="">
                                <div class="spacer-single"></div>
                                <strong class="text-red">INSTRUMENTACIÓN DE OBRAS SUBTERRÁNEAS.**</strong>
                                <br> Aula Heberto Castillo | 30 px
                                <br><br> Coordinador:
                                <br> Mario Arturo Aguillar Téllez
                                <br>
                                <a href="https://tidesa.com.mx/web2021/" target="blank">
                                    <strong class="text-yellow">TIDESA</strong>
                                </a>
                                <br><br>
                                <a href="archivos/precongreso/programa_4ºcurso_precongreso_V3.pdf" target="_blank"
                                    class="btn-custom text-white text-large">
                                    Programa
                                </a>
                            </div>

                        </div>

                    </div>
                </div>
            </section>
            <center>
                <a href="archivos/AMITOS_2023_plantilla_presentaciones.pptx"
                class="btn-custom text-white text-large">
                    Descargar plantilla para presentación
                </a>
            </center>
            <!-- FIN - CONTENIDOS MIERCOLES 29 DE NOVIEMBRE 5CMITOS WEB 2023 -->

        </div>
        <!-- FIN - CONTENIDOS CURSOS PRECONGRESO 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("include/footer.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

</body>

</html>