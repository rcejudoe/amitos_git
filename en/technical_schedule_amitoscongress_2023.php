<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "Technical schedule | 5th Mexican Congress Of Tunnel Engineering and Underground Works | november - december 2023 | Mexico City";
$description = "Technical schedule. 5th Mexican Congress Of Tunnel Engineering and Underground Works. November 29<sup>th</sup> and 30<sup>th</sup>, December 1 <sup>st</sup>, 2023. Mexico City";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2020 -->
    <? include_once("../include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2020 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/en/index.php">
                                    <img class="logo" src="../img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2020 -->
                                <? include_once("../include/navbar_en.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2020 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contact
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - SUBHEADER PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            TECHNICAL SCHEDULE
                        </h1>
                        <ul class="crumb">
                            <li>
                                <a href="<?= $servidor ?>/en/index.php">
                                    Home
                                </a>
                            </li>
                            <li class="sep">
                                /
                            </li>
                            <li>
                                <a href="programa_tecnico_congresoamitos_2023.php">
                                    Technical schedule
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - SUBHEADER PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->

        <!-- INICIO - CONTENIDOS PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->
        <div id="content" class="no-bottom no-top">

            <!-- INICIO - CONTENIDOS JUEVES 30 DE NOVIEMBRE 01 DICIEMBRE 5CMITOS WEB 2023 -->
            <section id="pricing-table">

                <div class="item pricing">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                                <h3>
                                    Thursday
                                    <br> 30th november 2023
                                </h3>
                                <div class="separator"><span><i class="fa fa-square"></i></span></div>
                            </div>
                        </div>

                        <div class="row">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-red text-center" width="160px">
                                            Time
                                        </th>
                                        <th scope="col" class="text-red text-center">

                                        </th>
                                        <!-- <th scope="col" class="text-red text-center">
                                            Organizador
                                        </th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            8:00 - 8:30
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Registration of attendees | Welcome coffee</strong>
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            8:30 - 9:00
                                        </th>
                                        <td class="text-center">
                                            <strong class="text-red">Opening</strong>
                                            <br>Sergio M. ALCOCER MARTÍNEZ DE CASTRO, AMITOS President, 
                                            <br> President of the Organizing Committee
                                        </td>
                                        <!-- <td class="text-center">
                                            Invitado especial, Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            9:00 - 9:45
                                        </th>
                                        <td class="text-center">
                                            Keynote lecture 1.1 
                                            <br> <strong>Integrated process for risk management of tunnel projects</strong>
                                            <br>Andrés, MARULANDA ESCOBAR ITA, First Vice-President | Colombia
                                            <br> <strong>Organizer:</strong> Technology and innovation in underground works engineering
                                            <br>
                                            Session Chairman Juan PAULÍN AGUIRRE
                                        </td>
                                        <!-- <td class="text-center">
                                            Juan Paulín Aguirre.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            9:45 - 10:30
                                        </th>
                                        <td class="text-center">
                                            Keynote lecture 1.2
                                            <br><strong>Implementation of directional drilling methods for unstable ground and hard rock conditions:
                                                <br>
                                                a cost-effective solution for challenging geologies in trenchless excavation applications</strong>
                                                <br>Roberto, ZILLANTE
                                                <br>
                                                PETRA, Chief Technology Officer | USA, Colombia
                                            <br> <strong>Organizer:</strong> Technology and innovation in underground works engineering
                                            <br>
                                            Session Chairman Juan PAULÍN AGUIRRE
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Juan Paulín Aguirre.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            10:30 - 10:55
                                        </th>
                                        <td class="text-center">
                                            Panelist 1.1
                                            <br>
                                            <strong>Advanced analytics applied to excavation risks with an EPB tunnel boring machine under high chamber pressures</strong>
                                            <br>
                                            Waldo, SALUD VELÁZQUEZ
                                            <br>
                                            SENER | Mexico, Spain
                                            <br> <strong>Organizer:</strong> Technology and innovation in underground works engineering
                                            <br>
                                            Session Chairman Juan PAULÍN AGUIRRE
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            10:55 - 11:20
                                        </th>
                                        <td class="text-center">
                                            Panelist 1.2
                                            <br>
                                            <strong>Continuous advance – developments for a new Herrenknecht TBM</strong>
                                            <br>
                                            Francisco A., AVILA ARANDA
                                            <br>
                                            Herrenknecht AG | Panama
                                            <br> <strong>Organizer:</strong> Technology and innovation in underground works engineering
                                            <br>
                                            Session Chairman Juan PAULÍN AGUIRRE

                                        </td>
                                        <!-- <td class="text-center">
                                            David Javier Juárez Flores.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            11:20 - 11:45
                                        </th>
                                        <td class="text-center">
                                            Panelist 1.3
                                            <br>
                                            Implementation of the BIM methodology in underground works – recent experiences
                                            <br>
                                            in large infrastructure projects
                                            <br>
                                            Eric, CARRERA
                                            <br>
                                            Lombardi SA | Switzerland
                                            <br> <strong>Organizer:</strong> Technology and innovation in underground works engineering
                                            <br>
                                            Session Chairman Juan PAULÍN AGUIRRE
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión David Javier Juárez Flores.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            11:45 - 12:05
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Coffee break | Expo and interviews</strong>
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            12:05 - 12:50
                                        </th>
                                        <td class="text-center">
                                            Keynote lecture 2.1
                                            <br>
                                            <strong>Industrias Peñoles -136 years of mining: challenges and opportunities for the development of national engineering</strong>
                                            <br>
                                            Moisés, DURÁN
                                            <br>
                                            Peñoles - Baluarte | Mexico
                                            <br> <strong>Organizer: </strong> Mining and underground works
                                            <br>
                                            Session Chairman David Javier JUÁREZ FLORES
                                        </td>
                                        <!-- <td class="text-center">
                                            Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            12:50 - 13:35
                                        </th>
                                        <td class="text-center">
                                            Keynote lecture 2.2
                                            <br>
                                            <strong>Mechanized systems for construction and development of underground mining</strong>
                                            <br>
                                            Patrick, RENNKAMP
                                            <br>
                                            Herrenknecht AG, Chief Technology Officer | Germany
                                            <br> <strong>Organizer: </strong> Mining and underground works
                                            <br>
                                            Session Chairman David Javier JUÁREZ FLORES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            13:35 - 14:20
                                        </th>
                                        <td class="text-center">
                                            Panelist 2.1
                                            <br>
                                            <strong>Emerging Techniques in Safe Shaft Construction, 
                                                <br> Cementation Americas Future of Shaft Deepening
                                            </strong>
                                            <br>
                                            Shawn Eric, STICKLER | Armando, ARMENDARIZ
                                            <br>
                                            Cementation Mining | Mexico
                                            <br> <strong>Organizer: </strong> Mining and underground works
                                            <br>
                                            Session Chairman David Javier JUÁREZ FLORES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            14:20 - 14:50
                                        </th>
                                        <td class="text-center">
                                            Panelist 2.2
                                            <br>
                                            <strong>Concrete with special requirements for mining works
                                            </strong>
                                            <br>
                                            Sergio Monserrat, OCAMPO TORRES
                                            <br>
                                            MAPEI | Mexico
                                            <br> <strong>Organizer: </strong> Mining and underground works
                                            <br>
                                            Session Chairman David Javier JUÁREZ FLORES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            14:50 - 16:20
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Lunchtime | Expo and interviews</strong>
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            16:20 - 17:25
                                        </th>
                                        <td class="text-center">
                                            Keynote lecture 3.1
                                            <br>
                                            <strong>The design of energy tunnels for sustainable cities
                                            </strong>
                                            <br>
                                            Marco, BARLA
                                            <br>
                                            Politecnico di Torino | Italy
                                            <br> <strong>Organizer: </strong> Numerical methods in underground works
                                            <br>
                                            Session Chairman Miguel Ángel MÁNICA MALCOM
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            17:25 - 17:50
                                        </th>
                                        <td class="text-center">
                                            Panelist 3.1
                                            <br>
                                            <strong>Impact of the modeling method and constitutive model in the analysis of underground rock works
                                            </strong>
                                            <br>
                                            Edgar, MONTIEL
                                            <br>
                                            SRK | Chile
                                            <br> <strong>Organizer: </strong> Numerical methods in underground works
                                            <br>
                                            Session Chairman Miguel Ángel MÁNICA MALCOM
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            17:50 - 18:15
                                        </th>
                                        <td class="text-center">
                                            Panelist 3.2
                                            <br>
                                            <strong>Seismic interaction between elevated and underground structures in soft soils
                                            </strong>
                                            <br>
                                            Juan Manuel, MAYORAL VILLA
                                            <br>
                                            Institute of Engineering, UNAM | Mexico
                                            <br> <strong>Organizer: </strong> Numerical methods in underground works
                                            <br>
                                            Session Chairman Miguel Ángel MÁNICA MALCOM
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            18:15 - 18:40
                                        </th>
                                        <td class="text-center">
                                            Panelist 3.3
                                            <br>
                                            <strong>Numerical modeling of urban excavations in soft soils
                                            </strong>
                                            <br>
                                            Felipe de Jesús, JIMÉNEZ RAMÍREZ
                                            <br>
                                            Engineering Postgraduate, UNAM | Mexico
                                            <br> <strong>Organizer: </strong> Numerical methods in underground works
                                            <br>
                                            Session Chairman Miguel Ángel MÁNICA MALCOM
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            18:40 - 19:05
                                        </th>
                                        <td class="text-center">
                                            Panelist 3.4
                                            <br>
                                            <strong>Numerical evaluation of the reliability of a microtunnel in soft soils in the Valley of Mexico
                                            </strong>
                                            <br>
                                            Valeria Laura, JÁQUEZ DOMÍNGUEZ
                                            <br>
                                            Institute of Engineering, UNAM | Mexico
                                            <br> <strong>Organizer: </strong> Numerical methods in underground works
                                            <br>
                                            Session Chairman Miguel Ángel MÁNICA MALCOM
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Miguel Ángel Mánica Malcom.
                                        </td> -->
                                    </tr>
                                </tbody>
                            </table>





                            <div class="row">
                                <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                                    <h3>
                                        Friday
                                        <br> 1st december 2023
                                    </h3>
                                    <div class="separator"><span><i class="fa fa-square"></i></span></div>
                                </div>
                            </div>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-red text-center" width="160px">
                                            Time
                                        </th>
                                        <th scope="col" class="text-red text-center">

                                        </th>
                                        <!-- <th scope="col" class="text-red text-center">
                                            Organizador
                                        </th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            8:00 - 8:30
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Welcome coffee | Expo and interviews</strong>
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            8:30 - 9:15
                                        </th>
                                        <td class="text-center">
                                            Keynote lecture 4.1
                                            <br>
                                            <strong>Geothermal energy and the world of tunnels</strong>
                                            <br>
                                            Carlos, LÓPEZ JIMENO 
                                            <br> Professor at the Polytechnic University of Madrid | Spain
                                            <br>
                                            <Strong>Organizer: </Strong> Geothermal energy and unique projects in underground works
                                            <br>
                                            Session Chairman José Francisco SUÁREZ FINO
                                        </td>
                                        <!-- <td class="text-center">
                                            José Francisco Suárez Fino.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            9:15 - 10:00
                                        </th>
                                        <td class="text-center">
                                            Keynote lecture 4.2
                                            <br>
                                            <strong>Tunnel lining in Scandinavia</strong>
                                            <br>
                                            Antonio, ALONSO JIMÉNEZ
                                            <br>
                                            CN Geological and Mining Institute of Spain, Higher Council for Scientific Research
                                            <br>
                                            <Strong>Organizer: </Strong> Geothermal energy and unique projects in underground works
                                            <br>
                                            Session Chairman José Francisco SUÁREZ FINO

                                        </td>
                                        <!-- <td class="text-center">
                                            <strong>Conferencistas:</strong> Presidente de Sesión José Francisco Suárez
                                            Fino.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            10:00 - 10:25
                                        </th>
                                        <td class="text-center">
                                            Panelist 4.1
                                            <br>
                                            <strong>New approach to tunnel design in densely populated urban areas</strong>
                                            <br>
                                            Juan Manuel, MAYORAL VILLA 
                                            <br> Institute of Engineering, UNAM | Mexico
                                            <br>
                                            <Strong>Organizer: </Strong> Geothermal energy and unique projects in underground works
                                            <br>
                                            Session Chairman José Francisco SUÁREZ FINO
                                        </td>
                                        <!-- <td class="text-center">

                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            10:25 - 10:50
                                        </th>
                                        <td class="text-center">
                                            Panelist 4.2
                                            <br>
                                            <strong>Geotechnical and structural monitoring system loading chamber and hydraulic tunnel 
                                               <br> in TBM – Riachuelo l3 – Buenos Aires</strong>
                                               <br>
                                               Carlo, VEZZOLI 
                                               <br> SISGEO LATINOAMÉRICA S.AS. | Colombia
                                               <br>
                                               <Strong>Organizer: </Strong> Geothermal energy and unique projects in underground works
                                            <br>
                                            Session Chairman José Francisco SUÁREZ FINO
                                        </td>
                                        <!-- <td class="text-center">
                                            Raymundo Gerardo González Reyes.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            10:50 - 11:15
                                        </th>
                                        <td class="text-center">
                                            Panelist 4.3
                                            <br>
                                            <strong>Court vs. Tunnel, Fake Tunnel</strong>
                                            <br>
                                            José Francisco, SUÁREZ FINO 
                                            <br> Consultec Ingeniería, Arquitectura y Supervisión | Mexico
                                            <br>
                                               <Strong>Organizer: </Strong> Geothermal energy and unique projects in underground works
                                            <br>
                                            Session Chairman José Francisco SUÁREZ FINO
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente de Sesión Raymundo Gerardo González Reyes.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            11:15 - 11:45
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Coffee break | Expo and interviews</strong>
                                        </td>
                                        <!-- <td class="text-center">
                                            Jardín del CICM, Jesús Díez Químico, Enólogo y Viticultor Mexicano | www.
                                            vinicultura.com.mx
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            11:45 - 12:30
                                        </th>
                                        <td class="text-center">
                                            Keynote lecture 5.1
                                            <br>
                                            <strong>Factors that define the execution of a project with trenchless technologies</strong>
                                            <br>
                                            Luis Guillermo, MALDONADO 
                                            <br> LASTT, Expresidente | Colombia
                                            <br>
                                            <strong>Organizer: </strong> Microtunnels and trenchless excavation technology
                                            <br>
                                            Session Chairman Raymundo Gerardo GONZÁLEZ REYES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            12:30 - 13:15
                                        </th>
                                        <td class="text-center">
                                            Keynote lecture 5.2
                                            <br>
                                            <strong>Tunnels and microtunnels to improve the sanitation system in preparation 
                                              <br>  for the Paris 2024 Olympic Games</strong>
                                            <br>
                                            Julien BRUNETON 
                                            <br> BESSAC Inc., Director | France
                                            <br>
                                            <strong>Organizer: </strong> Microtunnels and trenchless excavation technology
                                            <br>
                                            Session Chairman Raymundo Gerardo GONZÁLEZ REYES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            13:15 - 13:40
                                        </th>
                                        <td class="text-center">
                                            Panelist 5.1
                                            <br>
                                            <strong>Installation of shore access and subsea pipelines with trenchless 
                                               <br> methods: technologies and practical case studies</strong>
                                            <br>
                                            David, JUÁREZ 
                                            <br> Herrenknecht AG | Mexico
                                            <br>
                                            <strong>Organizer: </strong> Microtunnels and trenchless excavation technology
                                            <br>
                                            Session Chairman Raymundo Gerardo GONZÁLEZ REYES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            13:40 - 14:05
                                        </th>
                                        <td class="text-center">
                                            Panelist 5.2
                                            <br>
                                            <strong>The importance of the cutting tool for the good execution and construction of mechanized tunnels</strong>
                                            <br>
                                            Donhe Joao, FUENTES 
                                            <br> Palmieri | Mexico
                                            <br>
                                            <strong>Organizer: </strong> Microtunnels and trenchless excavation technology
                                            <br>
                                            Session Chairman Raymundo Gerardo GONZÁLEZ REYES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            14:05 - 14:30
                                        </th>
                                        <td class="text-center">
                                            Panelist 5.3
                                            <br>
                                            <strong>Digitization, automation and artificial intelligence the way to reduce construction costs</strong>
                                            <br>
                                            Jonatan, RODRÍGUEZ MARCOS 
                                            <br> VMT GmbH (Gesellshaft für Vermessungtechnik) | Spain
                                            <br>
                                            <strong>Organizer: </strong> Microtunnels and trenchless excavation technology
                                            <br>
                                            Session Chairman Raymundo Gerardo GONZÁLEZ REYES
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            14:30 - 15:00
                                        </th>
                                        <td class="text-center">
                                            Commemorative video for 40 years of AMITOS and tribute to the Mexican engineering that has written this history.
                                            <br>
                                            <strong>Organizer:</strong>
                                            AMITOS President, President of the Organizing Committee
                                        </td>
                                        <!-- <td class="text-center">
                                            Presidente AMITOS, Presidente Comité Organizador.
                                        </td> -->
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-center">
                                            15:00 - 17:30
                                        </th>
                                        <td class="text-center text-red">
                                            <strong>Closing lunch and special conference</strong>
                                            <br>
                                            CICM Garden, Jesús Díez Mexican Chemist, Oenologist and Viticulturist | www.
                                            vinicultura.com.mx
                                        
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        
                    </div>
                </div>
            </section>
            <center>
                <a href="archivos/AMITOS_2023_plantilla_presentaciones.pptx"
                class="btn-custom text-white text-large">
                    Download presentation template
                </a>
            </center>
            <!-- FIN - CONTENIDOS JUEVES 30 DE NOVIEMBRE 01 DICIEMBRE 5CMITOS WEB 2023 -->

        </div>
        <!-- FIN - CONTENIDOS PROGRAMA TÉCNICO 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("../include/footer_en.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("../include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

</body>

</html>