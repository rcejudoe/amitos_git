<!DOCTYPE html>

<!-- 
AMITOS CONGRESO - 2023
Dominio: www.amitoscongreso2023.com.mx
Fecha de inicio: abril 2023
Desarrollado por: Punto Zip
Web empresa: https://puntozip.com.mx/
-->

<?
$title = "Pricing | 5th Mexican Congress Of Tunnel Engineering and Underground Works | november - december 2023 | Mexico City";
$description = "Pricing. 5th Mexican Congress Of Tunnel Engineering and Underground Works. November 29<sup>th</sup> and 30<sup>th</sup>, December 1 <sup>st</sup>, 2023. Mexico City";
?>

<html lang="en">

<head>

    <!-- INICIO - HEADLINKS 5CMITOS WEB 2020 -->
    <? include_once("../include/head-links.php"); ?>
    <!-- FIN - HEADLINKS 5CMITOS WEB 2020 -->

</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- INICIO - HEADER 5CMITOS WEB 2023 -->
        <header class="transparent">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="column social">
                                <a href="https://www.facebook.com/people/Asociaci%C3%B3n-Mexicana-de-Ingenier%C3%ADa-de-T%C3%BAneles-y-Obras-Subterr%C3%A1neas-AC/100063587263342/" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="https://www.linkedin.com/company/asociaci%C3%B3n-mexicana-de-ingenier%C3%ADa-de-t%C3%BAneles-y-obras-subterr%C3%A1neas-a-c/" target="blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-flex">

                            <div id="logo">
                                <a href="<?= $servidor ?>/en/index.php">
                                    <img class="logo" src="../img/logo/logo_40_amitos_sf_2.webp" alt="">
                                </a>
                            </div>

                            <span id="menu-btn"></span>

                            <div class="md-flex-col">

                                <!-- INICIO - NAVBAR 5CMITOS WEB 2020 -->
                                <? include_once("../include/navbar_en.php"); ?>
                                <!-- FIN - NAVBAR 5CMITOS WEB 2020 -->

                            </div>

                            <div class="md-flex-col col-extra">
                                <div class="de_phone-simple">
                                    <i class="fa fa-email id-color"></i>
                                    <span class="id-color">
                                        Contact
                                    </span>
                                    <span class="d-num">
                                        <a href="mailto:amitos@amitos.org" class="text-blue-dark">
                                            amitos@amitos.org
                                        </a>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </header>
        <!-- FIN - HEADER 5CMITOS WEB 2023 -->

        <!-- INICIO - SUBHEADER COSTOS 5CMITOS WEB 2023 -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            Pricing
                        </h1>
                        <ul class="crumb">
                            <li>
                                <a href="<?= $servidor ?>/en/index.php">
                                    Home
                                </a>
                            </li>
                            <li class="sep">
                                /
                            </li>
                            <li>
                                <a href="pricing_amitoscongress_2023.php">
                                    Pricing
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- FIN - SUBHEADER COSTOS 5CMITOS WEB 2023 -->

        <!-- INICIO - CONTENIDOS COSTOS 5CMITOS WEB 2023 -->
        <div id="content" class="no-bottom no-top">

            <!-- INICIO - TABLAS COSTOS 5CMITOS WEB 2023 -->
            <section id="section-pricing-coworking">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="item pricing">
                                <div class="container">
                                    <div class="row">

                                        <!-- INICIO - TABLA INSCRIPCIÓN TEMPRANA COSTOS 5CMITOS WEB 2023 -->
                                        <!-- <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay="0s">
                                            <div class="pricing-s1 light mb30">
                                                <div class="top">
                                                    <h2>
                                                        <strong>Early registration</strong>
                                                    </h2>
                                                </div>
                                                <div class="bottom">
                                                    <ul>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Attendees <strong>(partners)</strong>
                                                            <strong class="text-red">| $275 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Attendees <strong>(non-members)</strong>
                                                            <strong class="text-red">| $400 USD </strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Students <strong>(face-to-face or zoom)</strong>
                                                            <strong class="text-red">| $37.50 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Attendee + course <strong>(partners)</strong>
                                                            <strong class="text-red">| $425 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Attendee + course <strong>(non members)</strong>
                                                            <strong class="text-red">| $675 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Student + course <strong>(face-to-face or zoom)</strong>
                                                            <strong class="text-red">| $100 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Only Course <strong>(partners)</strong>
                                                            <strong class="text-red">| $275 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Only Course <strong>(no members)</strong>
                                                            <strong class="text-red">| $400 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            <strong>ZOOM</strong> attendees*
                                                            <strong class="text-red">| $250 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            <strong>ZOOM</strong> Course*
                                                            <strong class="text-red">| $250 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Attendee + <strong>ZOOM Course*</strong>
                                                            <strong class="text-red">$400 USD</strong>
                                                        </li>
                                                    </ul>
                                                    <a href="https://5congresoamitos.com.mx/registro/" target="_blank" class="btn-custom text-white text-large">
                                                        ¡Sign up!
                                                    </a>
                                                </div>
                                            </div>
                                        </div> -->
                                        <!-- FIN - TABLA INSCRIPCIÓN TEMPRANA COSTOS 5CMITOS WEB 2023 -->

                                        <!-- INICIO - TABLA INSCRIPCIÓN NORMAL COSTOS 5CMITOS WEB 2023 -->
                                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay=".3s">
                                            <div class="pricing-s1 light mb30">
                                                <div class="top">
                                                    <h2>
                                                        <strong>Regular registration</strong>
                                                    </h2>
                                                </div>
                                                <div class="bottom">
                                                    <ul>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Attendees<strong>(socios)</strong> <strong class="text-red">| $400 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Attendees <strong>(non-members)</strong>
                                                            <strong class="text-red">| $525 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Students <strong>(face-to-face or zoom)</strong>
                                                            <strong class="text-red">| $37.50 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Attendee + course <strong>(partners)</strong>
                                                            <strong class="text-red">| $675 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Attendee + course <strong>(non-members) </strong>
                                                            <strong class="text-red">| $925 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Student + course <strong>(face-to-face or zoom)</strong>
                                                            <strong class="text-red">| $100 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Only Course <strong>(partners)</strong>
                                                            <strong class="text-red">| $400 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Only Course <strong>(no members)</strong>
                                                            <strong class="text-red">| $525 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            <strong>ZOOM</strong> attendees*
                                                            <strong class="text-red">| $375 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            <strong>ZOOM</strong> Course *
                                                            <strong class="text-red">| $375 USD</strong>
                                                        </li>
                                                        <li class="text-medium">
                                                            <i class="icon_check"></i>
                                                            Attendee + <strong>ZOOM Course*</strong>
                                                            <strong class="text-red">| $525 USD</strong>
                                                        </li>
                                                    </ul>
                                                    <a href="https://5congresoamitos.com.mx/registro/" target="_blank" class="btn-custom text-white text-large">
                                                        ¡Sign up!
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- FIN - TABLA INSCRIPCIÓN NORMAL COSTOS 5CMITOS WEB 2023 -->

                                        <p class="text-red text-large">
                                            * AMITOS members and National of International Societies with agreement, 10% discount.
                                        </p>

                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </section>
            <!-- FIN - TABLAS COSTOS 5CMITOS WEB 2023 -->

            <!-- INICIO - PRECIO STAND 5CMITOS WEB 2023 -->
            <section id="call-to-action" class="bg-color call-to-action padding40" aria-label="cta">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-7">
                            <h3 class="text-dark size-2 no-margin text-white">
                                Notes
                                <br> a. Prices plus taxes (VAT).
                                <!-- <br> b. Deadline for early registration, October 02, 2023. -->
                                <br> b. The partners considered as such are the AMITOS
                                members, and Nationals of Internationals Societies with
                                agreement.
                            </h3>
                        </div>
                    </div>
                </div>
            </section>
            <!-- FIN - PRECIO STAND 5CMITOS WEB 2023 -->
            <section>
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-7">
                            <h3 class="text-dark size-2 no-margin">
                                HOSPEDAJE | LODGMENT
                                <br>
                                <img src="../img/costos/logo-radisson.jpg" alt="Logo Hotel Radisson Perisur">
                                <br>
                                Agreement with the Hotel Paraíso Radisson Perisur
                                <br>
                                Reservations directly through e-mail: reservaciones@radisson.com.mx
                                <br>
                                Reservation code: <strong>“AMITOS2023”</strong>
                                <br>
                                At'n. Reservations Department Radisson Paraiso Hotel Mexico 55 5927 5959
                                <br>
                                Address: Cúspide 53, Parques del Pedregal, Tlalpan, 14010. Mexico City, Mexico
                            </h3>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- FIN - CONTENIDOS COSTOS 5CMITOS WEB 2023 -->

        <!-- INICIO - FOOTER 5CMITOS 2023 -->
        <? include_once("../include/footer_en.php"); ?>
        <!-- FIN - FOOTER 5CMITOS 2023 -->

    </div>

    <!-- INICIO - JSS 5CMITOS 2023 -->
    <? include_once("../include/jss.php"); ?>
    <!-- FIN - JSS 5CMITOS 2023 -->

</body>

</html>