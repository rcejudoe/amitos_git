<nav class="md-flex">
    <ul id="mainmenu">

        <li>
            <a href="#">
                Committees
            </a>
            <ul>
                <li>
                    <a href="<?= $servidor ?>/en/committees_amitoscongress_2023.php">
                        Organizing Committee
                    </a>
                </li>
                <li>
                    <a href="<?= $servidor ?>/en/session_chairs_amitoscongress_2023.php">
                        Session Chairs
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="#">
                Program
            </a>
            <ul>
                <li>
                    <a href="<?= $servidor ?>/en/preconference_courses_amitoscongress_2023.php">
                        Preconference courses
                    </a>
                </li>
                <li>
                    <a href="<?= $servidor ?>/en/precongress_conferences_amitoscongress_2023.php">
                        Precongress Conferences
                    </a>
                </li>
                <li>
                    <a href="<?= $servidor ?>/en/technical_schedule_amitoscongress_2023.php">
                        Technical schedule
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="<?= $servidor ?>/en/expo_amitoscongress_2023.php">
                Expo
            </a>
            <!--<ul>
                <li>
                    <a href="#">
                        Residential Design
                    </a>
                </li>
                <li>
                    <a href="#">
                        Hospitaly Design
                    </a>
                </li>
                <li>
                    <a href="#">
                        Office Design
                    </a>
                </li>
                <li>
                    <a href="#">
                        Commercial Design
                    </a>
                </li>
                <li>
                    <a href="#">
                        All Services
                    </a>
                </li>
                <li>
                    <a href="#">
                        New All Services
                    </a>
                </li>
            </ul>-->
        </li>

        <li>
            <a href="<?= $servidor ?>/en/pricing_amitoscongress_2023.php">
                Pricing
            </a>
        </li>

        <li>
            <a href="<?= $servidor ?>/en/contact_amitoscongress_2023.php">
                Contact
            </a>
        </li>

        <li>
            <a href="https://5congresoamitos.com.mx/registro/" target="_blank">
                Registration
            </a>
        </li>

        <li>
            <a href="../boletin/boletin_5congresoamitos_noviembre_2023_v3.pdf" download>
                Bulletin
            </a>
        </li>

        <li>
            <a href="../index.php">
                ES
            </a>
        </li>

    </ul>
</nav>
<!-- mainmenu close -->

